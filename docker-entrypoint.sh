#!/bin/bash

set -e
_run_secrets="/scripts/run-secrets.sh"
if [[ -f "${_run_secrets}" ]]
   then
    chmod +x $_run_secrets
    source $_run_secrets
fi   
exec "$@"
