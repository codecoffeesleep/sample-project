FROM node:14-alpine

WORKDIR /app

ADD package.json /app
RUN npm install

ADD . /app
RUN apk add --no-cache bash
RUN chmod +x /app/docker-entrypoint.sh
RUN npm run build

ENTRYPOINT ["/app/docker-entrypoint.sh"]
CMD ["npm", "run", "start:prod"]
