-- CREATE EXTENSION cube;
-- CREATE EXTENSION earthdistance;

drop table user_notifications;
drop table notifications;
drop table sms;
drop table migrations;
drop table contacts;
drop table transactions;
drop table invoices;
drop table qrs;
drop table wallets;
drop table company_user;
drop table merchant_user;
drop table merchants;
drop table companies;
drop table users;

-- SELECT *, point(40.994921, 29.121858) <@> (point(pt)::point) as distance
-- FROM merchants
-- WHERE (point(40.994921, 29.121858) <@> point(pt)) < 0.1
-- ORDER BY distance ASC;


SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = 'jafar';



==================================>>>
migration stuffs:
// import { MigrationInterface, QueryRunner } from 'typeorm';
//
// export class InvoiceRefactoring1590590950231 implements MigrationInterface {
//
//   public async up(queryRunner: QueryRunner): Promise<void> {
//     await queryRunner.query(`ALTER TABLE "invoice" RENAME COLUMN "amount" TO "amounts"`);
//   }
//
//   public async down(queryRunner: QueryRunner): Promise<void> {
//     await queryRunner.query(`ALTER TABLE "invoice" RENAME COLUMN "amounts" TO "amount"`);
//   }
//
// }

typeorm migration:create -n InvoiceRefactoring
ts-node ./node_modules/typeorm/cli.js migration:run
