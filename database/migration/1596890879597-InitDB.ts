import { MigrationInterface, QueryRunner } from 'typeorm';
import { Table }                           from 'typeorm/index';

export class CreateAllTables1596890879597 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {

    await queryRunner.createTable(new Table({
      name: 'users',
      columns: [
        {
          name: 'id',
          type: 'serial',
          isPrimary: true,
          isNullable: false,
          generationStrategy: 'increment',
          unsigned: true,
        },
        {
          name: 'first_name',
          type: 'varchar(255)',
          isNullable: true,
        },
        {
          name: 'last_name',
          type: 'varchar(255)',
          isNullable: true,
        },
        {
          name: 'mobile_number',
          type: 'varchar(20)',
          isNullable: false,
          isUnique: true,
        },
        {
          name: 'image',
          type: 'varchar(255)',
          isNullable: true,
        },
        {
          name: 'username',
          type: 'varchar(100)',
          isNullable: true,
          isUnique: true,
        },
        {
          name: 'password',
          type: 'varchar(100)',
          isNullable: true,
        },
        {
          name: 'refresh_token',
          type: 'varchar(500)',
          isNullable: true,
        },
        {
          name: 'created_at',
          isNullable: false,
          type: 'timestamp with time zone',
          default: 'timezone(\'Europe/Istanbul\'::text, now())',
        },
        {
          name: 'updated_at',
          isNullable: false,
          type: 'timestamp with time zone',
          default: 'timezone(\'Europe/Istanbul\'::text, now())',
        },
      ],
    }), true, true, true);

    await queryRunner.createTable(new Table({
      name: 'merchants',
      columns: [
        {
          name: 'id',
          type: 'serial',
          isPrimary: true,
          isNullable: false,
          generationStrategy: 'increment',
          unsigned: true,
        },
        {
          name: 'name',
          type: 'varchar(255)',
          isNullable: false,
        },
        {
          name: 'logo',
          type: 'varchar(255)',
          isNullable: true,
        },
        {
          name: 'phone_number',
          type: 'varchar(255)',
          isNullable: true,
        },
        {
          name: 'settlement_dates',
          type: 'varchar(255)',
          isNullable: true,
        },
        {
          name: 'address',
          type: 'varchar(255)',
          isNullable: true,
        },
        {
          name: 'location',
          type: 'Point',
          spatialFeatureType: 'Point',
          srid: 4326,
          isNullable: true,
        },
        {
          name: 'created_at',
          isNullable: false,
          type: 'timestamp with time zone',
          default: 'timezone(\'Europe/Istanbul\'::text, now())',
        },
        {
          name: 'updated_at',
          isNullable: false,
          type: 'timestamp with time zone',
          default: 'timezone(\'Europe/Istanbul\'::text, now())',
        },
      ],
    }), true, true, true);

    await queryRunner.createTable(new Table({
      name: 'companies',
      columns: [
        {
          name: 'id',
          type: 'serial',
          isPrimary: true,
          isNullable: false,
          generationStrategy: 'increment',
          unsigned: true,
        },
        {
          name: 'name',
          type: 'varchar(255)',
          isNullable: false,
        },
        {
          name: 'legal_name',
          type: 'varchar(255)',
          isNullable: true,
        },
        {
          name: 'logo',
          type: 'varchar(255)',
          isNullable: true,
        },
        {
          name: 'phone_number',
          type: 'varchar(255)',
          isNullable: true,
        },
        {
          name: 'address',
          type: 'varchar(255)',
          isNullable: true,
        },
        {
          name: 'location',
          type: 'Point',
          spatialFeatureType: 'Point',
          srid: 4326,
          isNullable: true,
        },
        {
          name: 'created_at',
          isNullable: false,
          type: 'timestamp with time zone',
          default: 'timezone(\'Europe/Istanbul\'::text, now())',
        },
        {
          name: 'updated_at',
          isNullable: false,
          type: 'timestamp with time zone',
          default: 'timezone(\'Europe/Istanbul\'::text, now())',
        },
      ],
    }), true, true, true);

    await queryRunner.createTable(new Table({
      name: 'company_user',
      columns: [
        {
          name: 'id',
          type: 'serial',
          isPrimary: true,
          isNullable: false,
          generationStrategy: 'increment',
          unsigned: true,
        },
        {
          name: 'company_id',
          type: 'int',
          unsigned: true,
          isNullable: false,
        },
        {
          name: 'user_id',
          type: 'int',
          unsigned: true,
          isNullable: false,
        },
        {
          name: 'role',
          type: 'varchar(255)',
          isNullable: false,
        },
        {
          name: 'created_at',
          isNullable: false,
          type: 'timestamp with time zone',
          default: 'timezone(\'Europe/Istanbul\'::text, now())',
        },
        {
          name: 'updated_at',
          isNullable: false,
          type: 'timestamp with time zone',
          default: 'timezone(\'Europe/Istanbul\'::text, now())',
        },
      ],
      indices: [
        {
          columnNames: ['company_id'],
        },
        {
          columnNames: ['user_id'],
        },
      ],
    }), true, true, true);

    await queryRunner.createTable(new Table({
      name: 'merchant_user',
      columns: [
        {
          name: 'id',
          type: 'serial',
          isPrimary: true,
          isNullable: false,
          generationStrategy: 'increment',
          unsigned: true,
        },
        {
          name: 'merchant_id',
          type: 'int',
          unsigned: true,
          isNullable: false,
        },
        {
          name: 'user_id',
          type: 'int',
          unsigned: true,
          isNullable: false,
        },
        {
          name: 'role',
          type: 'varchar(255)',
          isNullable: false,
        },
        {
          name: 'created_at',
          isNullable: false,
          type: 'timestamp with time zone',
          default: 'timezone(\'Europe/Istanbul\'::text, now())',
        },
        {
          name: 'updated_at',
          isNullable: false,
          type: 'timestamp with time zone',
          default: 'timezone(\'Europe/Istanbul\'::text, now())',
        },
      ],
      indices: [
        {
          columnNames: ['merchant_id'],
        },
        {
          columnNames: ['user_id'],
        },
      ],
    }), true, true, true);

    await queryRunner.createTable(new Table({
      name: 'wallets',
      columns: [
        {
          name: 'id',
          type: 'serial',
          isPrimary: true,
          isNullable: false,
          generationStrategy: 'increment',
          unsigned: true,
        },
        {
          name: 'user_id',
          type: 'int',
          unsigned: true,
          isNullable: true,
        },
        {
          name: 'company_id',
          type: 'int',
          unsigned: true,
          isNullable: true,
          default: null,
        },
        {
          name: 'employee_no',
          type: 'varchar(50)',
          isNullable: true,
          default: null,
        },
        {
          name: 'merchant_id',
          type: 'int',
          unsigned: true,
          isNullable: true,
          default: null,
        },
        {
          name: 'balance',
          type: 'float',
          unsigned: true,
          default: 0,
        },
        {
          name: 'type',
          type: 'varchar(30)',
          isNullable: false,
        },
        {
          name: 'status',
          type: 'varchar(30)',
          isNullable: false,
        },
        {
          name: 'department',
          type: 'varchar(255)',
          isNullable: true,
          default: null,
        },
        {
          name: 'title',
          type: 'varchar(255)',
          isNullable: true,
          default: null,
        },
        {
          name: 'balance_per_month',
          type: 'float',
          isNullable: true,
          default: null,
        },
        {
          name: 'created_at',
          isNullable: false,
          type: 'timestamp with time zone',
          default: 'timezone(\'Europe/Istanbul\'::text, now())',
        },
        {
          name: 'updated_at',
          isNullable: false,
          type: 'timestamp with time zone',
          default: 'timezone(\'Europe/Istanbul\'::text, now())',
        },
      ],
      indices: [
        {
          columnNames: ['user_id'],
        },
        {
          columnNames: ['company_id'],
        },
        {
          columnNames: ['merchant_id'],
        },
      ],
    }), true, true, true);

    await queryRunner.createTable(new Table({
      name: 'qrs',
      columns: [
        {
          name: 'id',
          type: 'serial',
          isPrimary: true,
          isNullable: false,
          generationStrategy: 'increment',
          unsigned: true,
        },
        {
          name: 'merchant_id',
          type: 'int',
          unsigned: true,
          isNullable: false,
        },
        {
          name: 'default_amount',
          type: 'float',
          unsigned: true,
          default: 0,
        },
        {
          name: 'type',
          type: 'varchar(20)',
          isNullable: false,
        },
        {
          name: 'version',
          type: 'varchar(10)',
          isNullable: true,
          default: '1',
        },
        {
          name: 'code',
          type: 'varchar(20)',
          isNullable: false,
        },
        {
          name: 'expired_at',
          isNullable: true,
          type: 'timestamp with time zone',
          default: 'timezone(\'Europe/Istanbul\'::text, now())',
        },
        {
          name: 'created_at',
          isNullable: false,
          type: 'timestamp with time zone',
          default: 'timezone(\'Europe/Istanbul\'::text, now())',
        },
        {
          name: 'updated_at',
          isNullable: false,
          type: 'timestamp with time zone',
          default: 'timezone(\'Europe/Istanbul\'::text, now())',
        },
      ],
      indices: [
        {
          columnNames: ['merchant_id'],
        },
      ],
    }), true, true, true);

    await queryRunner.createTable(new Table({
      name: 'transactions',
      columns: [
        {
          name: 'id',
          type: 'serial',
          isPrimary: true,
          isNullable: false,
          generationStrategy: 'increment',
          unsigned: true,
        },
        {
          name: 'type',
          type: 'varchar(20)',
          isNullable: false,
        },
        {
          name: 'qr_id',
          type: 'int',
          unsigned: true,
          isNullable: false,
        },
        {
          name: 'ref_number',
          type: 'varchar(20)',
          isNullable: true,
          default: null,
        },
        {
          name: 'is_added_to_source',
          type: 'bool',
          isNullable: true,
          default: null,
        },
        {
          name: 'invoice_id',
          type: 'int',
          unsigned: true,
          isNullable: true,
        },
        {
          name: 'source_id',
          type: 'int',
          unsigned: true,
          isNullable: false,
        },
        {
          name: 'destination_id',
          type: 'int',
          unsigned: true,
          isNullable: false,
        },
        {
          name: 'source_before',
          type: 'float',
          unsigned: true,
          isNullable: false,
        },
        {
          name: 'source_after',
          type: 'float',
          unsigned: true,
          isNullable: false,
        },
        {
          name: 'destination_before',
          type: 'float',
          unsigned: true,
          isNullable: false,
        },
        {
          name: 'destination_after',
          type: 'float',
          unsigned: true,
          isNullable: false,
        },
        {
          name: 'amount',
          type: 'float',
          unsigned: true,
          default: 0,
        },
        {
          name: 'description',
          type: 'jsonb',
          isNullable: true,
          default: null,
        },
        {
          name: 'created_at',
          isNullable: false,
          type: 'timestamp with time zone',
          default: 'timezone(\'Europe/Istanbul\'::text, now())',
        },
        {
          name: 'updated_at',
          isNullable: false,
          type: 'timestamp with time zone',
          default: 'timezone(\'Europe/Istanbul\'::text, now())',
        },
      ],
      indices: [
        {
          columnNames: ['qr_id'],
        },
        {
          columnNames: ['source_id'],
        },
        {
          columnNames: ['destination_id'],
        },
      ],
    }), true, true, true);

    await queryRunner.createTable(new Table({
      name: 'tickets',
      columns: [
        {
          name: 'id',
          type: 'serial',
          isPrimary: true,
          isNullable: false,
          generationStrategy: 'increment',
          unsigned: true,
        },
        {
          name: 'user_id',
          type: 'int',
          unsigned: true,
          isNullable: false,
        },
        {
          name: 'status',
          type: 'varchar(20)',
          isNullable: false,
        },
        {
          name: 'message',
          type: 'text',
          isNullable: true,
          default: null,
        },
        {
          name: 'created_at',
          isNullable: false,
          type: 'timestamp with time zone',
          default: 'timezone(\'Europe/Istanbul\'::text, now())',
        },
        {
          name: 'updated_at',
          isNullable: false,
          type: 'timestamp with time zone',
          default: 'timezone(\'Europe/Istanbul\'::text, now())',
        },
      ],
      indices: [
        {
          columnNames: ['user_id'],
        },
        {
          columnNames: ['status'],
        },
      ],
    }), true, true, true);

    await queryRunner.createTable(new Table({
      name: 'notifications',
      columns: [
        {
          name: 'id',
          type: 'serial',
          isPrimary: true,
          isNullable: false,
          generationStrategy: 'increment',
          unsigned: true,
        },
        {
          name: 'user_id',
          type: 'int',
          unsigned: true,
          isNullable: false,
        },
        {
          name: 'status',
          type: 'varchar(20)',
          isNullable: false,
        },
        {
          name: 'is_read',
          type: 'varchar(20)',
          isNullable: false,
        },
        {
          name: 'title',
          type: 'varchar(500)',
          isNullable: false,
        },
        {
          name: 'message',
          type: 'text',
          isNullable: true,
          default: null,
        },
        {
          name: 'created_at',
          isNullable: false,
          type: 'timestamp with time zone',
          default: 'timezone(\'Europe/Istanbul\'::text, now())',
        },
        {
          name: 'updated_at',
          isNullable: false,
          type: 'timestamp with time zone',
          default: 'timezone(\'Europe/Istanbul\'::text, now())',
        },
      ],
      indices: [
        {
          columnNames: ['user_id'],
        },
        {
          columnNames: ['status'],
        },
      ],
    }), true, true, true);

    await queryRunner.createTable(new Table({
      name: 'sms',
      columns: [
        {
          name: 'id',
          type: 'serial',
          isPrimary: true,
          isNullable: false,
          generationStrategy: 'increment',
          unsigned: true,
        },
        {
          name: 'mobile_number',
          type: 'varchar(20)',
          unsigned: true,
          isNullable: false,
        },
        {
          name: 'body',
          type: 'text',
          isNullable: false,
        },
        {
          name: 'provider',
          type: 'varchar(100)',
          isNullable: false,
        },
        {
          name: 'type',
          type: 'varchar(20)',
          isNullable: false,
        },
        {
          name: 'sid',
          type: 'varchar(50)',
          isNullable: true,
          default: null,
        },
        {
          name: 'created_at',
          isNullable: false,
          type: 'timestamp with time zone',
          default: 'timezone(\'Europe/Istanbul\'::text, now())',
        },
        {
          name: 'updated_at',
          isNullable: false,
          type: 'timestamp with time zone',
          default: 'timezone(\'Europe/Istanbul\'::text, now())',
        },
      ],
      indices: [
        {
          columnNames: ['mobile_number'],
        },
      ],
    }), true, true, true);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "notifications"`);
    await queryRunner.query(`DROP TABLE "sms"`);
    await queryRunner.query(`DROP TABLE "tickets"`);
    await queryRunner.query(`DROP TABLE "transactions"`);
    await queryRunner.query(`DROP TABLE "qrs"`);
    await queryRunner.query(`DROP TABLE "wallets"`);
    await queryRunner.query(`DROP TABLE "company_user"`);
    await queryRunner.query(`DROP TABLE "merchant_user"`);
    await queryRunner.query(`DROP TABLE "merchants"`);
    await queryRunner.query(`DROP TABLE "companies"`);
    await queryRunner.query(`DROP TABLE "users"`);
  }

}
