// user.factory.ts
import { define } from 'typeorm-seeding';
import { User }   from '../../../src/users/user.entity';
import * as Faker from 'faker';

define(User, (faker: Faker.FakerStatic) => {
  const gender = faker.random.number(1);
  const firstName = faker.name.firstName(gender);
  const lastName = faker.name.lastName(gender);
  const mobileNumber = faker.phone.phoneNumber();

  const user = new User();
  user.firstName = `${ firstName } ${ lastName }`;
  user.lastName = faker.random.word();
  user.mobileNumber = mobileNumber;
  return user;
});
