// notification.factory.ts
import { define } from 'typeorm-seeding';
import { Notification }   from '../../../src/notifications/notification.entity';
import * as Faker from 'faker';

define(Notification, (faker: Faker.FakerStatic) => {
  const notification = new Notification();
  notification.title = faker.random.words(2);
  notification.message = faker.random.words(8);
  notification.read = true;
  notification.status = 'SENT'
  notification.userId = 20
  return notification;
});
