import { define }   from 'typeorm-seeding';
import * as Faker   from 'faker';
import { Merchant } from '../../../src/merchants/merchant.entity';

define(Merchant, (faker: Faker.FakerStatic) => {
  const name = faker.company.companyName();
  const logo = faker.image.business();
  const address = `${ faker.address.streetAddress() }, ${ faker.address.city() }, Turkey`;
  const phoneNumber = faker.phone.phoneNumber();

  const merchant = new Merchant();
  merchant.name = name;
  merchant.logo = logo;
  merchant.address = address;
  merchant.phoneNumber = phoneNumber;
  return merchant;
});
