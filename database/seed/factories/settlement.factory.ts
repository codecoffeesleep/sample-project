// settlement.factory.ts
import { define } from 'typeorm-seeding';
import { Invoice }   from '../../../src/invoices/invoice.entity';
import InvoiceStatuses   from '../../../src/invoices/constants/invoiceStatuses.enum';
import InvoiceTypes   from '../../../src/invoices/constants/invoiceTypes.enum';
import * as Faker from 'faker';

const randomProperty = function (obj) {
  const keys = Object.keys(obj);
  return obj[keys[ keys.length * Math.random() << 0]];
};

define(Invoice, (faker: Faker.FakerStatic) => {
  const invoice = new Invoice();
  invoice.amount = faker.random.number();
  invoice.status = randomProperty(InvoiceStatuses);
  invoice.type = randomProperty(InvoiceTypes);
  invoice.transactionNumber = faker.random.number();
  invoice.fromDate = faker.date.between('2020-01-01', '2020-01-05');
  invoice.toDate = faker.date.between('2020-01-05', '2020-01-07');
  invoice.description = { IBAN: '1223344555566', referenceNumber: '2423423423'};
  return invoice;
});
