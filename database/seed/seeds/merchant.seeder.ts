import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';
import { Merchant }   from '../../../src/merchants/merchant.entity';

export default class MerchantSeeder implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    await factory(Merchant)().createMany(20);
  }
}
