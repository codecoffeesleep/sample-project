import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';
import { Notification }       from '../../../src/notifications/notification.entity';

export default class NotificationSeeder implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    await factory(Notification)().createMany(100);
  }
}
