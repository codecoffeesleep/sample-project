import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';
import { Invoice }       from '../../../src/invoices/invoice.entity';

export default class SettlementSeeder implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    await factory(Invoice)().createMany(10);
  }
}
