import fs = require('fs');
import { configService } from '../../src/config.service';
import { join }          from 'path';

/*
 * These options are about database seeding.
 * Because of TypeOrmModuleOptions does not support seeding options,
 * we've to use this kind of solution.
 *
 */
const seedingOptions = {
  seeds: [
    join(__dirname, '../seed/seeds', '**', '*.seeder.ts'),
  ],
  factories: [
    join(__dirname, '../seed/factories', '**', '*.factory.ts'),
  ],
};

fs.writeFileSync(
  'ormconfig.json',
  JSON.stringify(Object.assign(configService.getTypeOrmConfig(), seedingOptions), null, 2), // last parameter can be changed based on how you want the file indented
);
