import { forwardRef, Module }          from '@nestjs/common';
import { AuthController }              from './auth.controller';
import { AuthService }                 from './auth.service';
import { UsersModule }                 from '../users/users.module';
import { PassportModule }              from '@nestjs/passport';
import { JwtModule }                   from '@nestjs/jwt';
import { JwtStrategy }                 from './jwt.strategy';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { SmsModule }                   from '../sms/sms.module';
import AttemptService                  from './attempt.service';

@Module({
  imports: [
    SmsModule,
    forwardRef(() => UsersModule),
    ConfigModule,
    PassportModule.register({
      defaultStrategy: 'jwt',
    }),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        secret: configService.get<string>('ACCESS_TOKEN_SECRET_KEY'),
        signOptions: {
          expiresIn: configService.get<string>('TOKEN_EXPIRES_IN'),
        },
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [AuthController],
  providers: [
    AuthService,
    JwtStrategy,
    AttemptService,
  ],
  exports: [
    JwtStrategy,
    PassportModule,
  ],
})
export class AuthModule {
}
