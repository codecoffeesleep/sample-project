import { PipeTransform, Injectable, ArgumentMetadata } from '@nestjs/common';
import SendCodeDto                                     from '../dto/send-code.dto';

@Injectable()
export class ModifyMobileNumberPipe implements PipeTransform<SendCodeDto, string> {
  transform(value: SendCodeDto, metadata: ArgumentMetadata): string {
    let countryCode = value['countryCode'];
    let mobileNumber = value['mobileNumber'];

    /*
     * Remove All 0 from the start of mobileNumber: e.g:
     * 05538458466 --> 5538458466
     */
    while (mobileNumber.startsWith('0')) {
      mobileNumber = mobileNumber.substr(1);
    }

    /*
     * Remove all 0 from the start of countryCode, e.g:
     * 0090 --> +90
     *
     */
    while (countryCode.startsWith('0')) {
      countryCode = countryCode.substring(1);
    }

    // Concat countryCode and mobileNumber, then add '+' at the start of that
    let modifiedMobileNumber = `${ countryCode }${ mobileNumber }`;
    if (!countryCode.startsWith('+')) {
      modifiedMobileNumber = `+${ modifiedMobileNumber }`;
    }

    return modifiedMobileNumber;
  }
}
