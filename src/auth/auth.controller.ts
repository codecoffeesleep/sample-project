import { Body, Controller, HttpStatus, Post, Req } from '@nestjs/common';
import SendCodeDto                                 from './dto/send-code.dto';
import { AuthService }                             from './auth.service';
import { ModifyMobileNumberPipe }                  from './pipes/modify-mobileNumber.pipe';
import CheckCodeDto                                from './dto/check-code.dto';
import { Request }                                 from 'express';
import { ControllerResponse }                      from '../common/response-decorator/responses.interface';
import { I18n, I18nContext, I18nService }          from 'nestjs-i18n';
import { UsersService }                            from '../users/users.service';


@Controller('auth')
export class AuthController {

  /**
   *
   * @param authService
   * @param i18n
   * @param userService
   */
  constructor(
    private readonly authService: AuthService,
    private readonly i18n: I18nService,
    private readonly userService: UsersService,
  ) {
  }

  /**
   *
   * @param sendCodeDto
   * @param i18n
   */
  @Post('send-code')
  public async sendCode(
    @Body(new ModifyMobileNumberPipe()) sendCodeDto: SendCodeDto,
    @I18n() i18n: I18nContext,
  ): Promise<ControllerResponse> {
    const modifiedMobileNumber = sendCodeDto.toString();
    const data = await this.authService.sendVerificationCode(modifiedMobileNumber);
    return {
      httpStatus: HttpStatus.CREATED,
      data,
      message: await i18n.translate('auth.SMS_SENT', {
        args: {
          mobileNumber: data.mobileNumber,
        },
      }),
    };
  }

  /**
   *
   * @param checkCodeDto
   * @param request
   */
  @Post('verify')
  public async verifyCode(@Body() checkCodeDto: CheckCodeDto, @Req() request: Request) {
    const user = await this.authService.checkVerificationCode(checkCodeDto);

    // TODO: recognize the device
    //
    // import * as DeviceDetector           from 'device-detector-js';
    //
    //
    // const deviceDetector = new DeviceDetector();
    // const device = deviceDetector.parse(request.get('user-agent'));
    //
    // console.log('------------------------>');
    // // https://www.npmjs.com/package/device-detector-js
    // console.log(device);
    // console.log('------------------------>');
    // // https://www.npmjs.com/package/express-useragent
    // console.log(request['useragent']);
    // console.log('------------------------>');
    if (checkCodeDto.scope === 'MERCHANT') {
      user['merchants'] = await this.userService.getRelatedMerchants(user.id);
    }

    return {
      data: user,
    };
  }

  /**
   *
   * @param request
   */
  @Post('refresh')
  public async refreshToken(@Req() request: Request) {
  }

  /**
   *
   */
  @Post('logout')
  public async logout(@I18n() i18n: I18nContext): Promise<ControllerResponse> {
    return {
      message: await i18n.translate('auth.LOGOUT_SUCCESSFULLY'),
    };
  }
}
