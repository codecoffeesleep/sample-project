import { Injectable }               from '@nestjs/common';
import * as bluebird                from 'bluebird';
import { ConfigService }            from '@nestjs/config';
import * as cryptoRandomString      from 'crypto-random-string';
import { TooManyAttemptsException } from '../common/exceptions/too-many-attempts.exception';

interface IAttempt {
  attemptsCount?: number,
  lastAttemptAt?: number,
  code?: string,
  isValidTill?: number
}

@Injectable()
export default class AttemptService {

  private readonly redisClient: any;
  static MAX_ATTEMPTS_COUNT: number;
  static ATTEMPT_CODE_LENGTH: number;
  static ATTEMPT_LIFE_TIME_IN_MINUTES: number;
  static ATTEMPT_LIMIT_AUTO_DELETE_IN_SECONDS: number;

  constructor() {
    const redis = require('redis');
    bluebird.promisifyAll(redis);

    const configService: ConfigService = new ConfigService();

    this.redisClient = redis.createClient({
      port: configService.get<number>('REDIS_PORT'),
      host: configService.get<string>('REDIS_HOST'),
      auth_pass: configService.get<string>('REDIS_PASSWORD'),
      no_ready_check: true,
    });

    AttemptService.MAX_ATTEMPTS_COUNT = +configService.get<number>('MAX_ATTEMPTS_COUNT');
    AttemptService.ATTEMPT_CODE_LENGTH = +configService.get<number>('ATTEMPT_CODE_LENGTH');
    AttemptService.ATTEMPT_LIFE_TIME_IN_MINUTES = +configService.get<number>('ATTEMPT_LIFE_TIME_IN_MINUTES');
    AttemptService.ATTEMPT_LIMIT_AUTO_DELETE_IN_SECONDS = +configService.get<number>('ATTEMPT_LIMIT_AUTO_DELETE_IN_SECONDS');
  }

  /**
   * Store new IAttempt for a mobileNumber
   *
   * @param mobileNumber
   */
  async storeNewAttempt(mobileNumber: string): Promise<string | false> {

    // Check the use can do new attempt or not
    const lastAttempt = await this.canAttempt(mobileNumber);

    // Initialize a new attempt
    const newAttempt = this.generateNewAttempt(lastAttempt);

    // Store into redis
    const isStoredIntoRedis = await this.redisClient.hmsetAsync(mobileNumber, newAttempt);
    if (isStoredIntoRedis === 'OK') {
      // AutoExpire
      this.redisClient.expire(mobileNumber, AttemptService.ATTEMPT_LIMIT_AUTO_DELETE_IN_SECONDS);
      return newAttempt.code;
    }
    return false;

  }

  /**
   * Check the received code is valid or not
   *
   * @param mobileNumber: string
   * @param code: string
   */
  public async checkLastAttempt(mobileNumber: string, code: string): Promise<boolean> {
    const lastAttempt = await this.getLastAttempt(mobileNumber);
    return lastAttempt && lastAttempt.code === code;
  }

  /**
   * According to the configurations,
   * check this user can submit new
   * attempt or not
   *
   * @param mobileNumber
   */
  private async canAttempt(mobileNumber: string): Promise<IAttempt> {
    const lastAttempt = await this.getLastAttempt(mobileNumber);
    if (lastAttempt) {
      const currentAttemptsCount = lastAttempt.attemptsCount;
      if (currentAttemptsCount >= AttemptService.MAX_ATTEMPTS_COUNT) {
        throw new TooManyAttemptsException('Your attempts limit was exceeded! Please try later.');
      }
    }
    return lastAttempt;
  }

  /**
   * Get the last attempt for this user
   *
   * @param mobileNumber
   */
  private async getLastAttempt(mobileNumber: string): Promise<any> {
    return await this.redisClient.hgetallAsync(mobileNumber);
  }

  /**
   * Based on previous attempt, generate a new attempt
   *
   */
  private generateNewAttempt(lastAttempt?: IAttempt): IAttempt {
    return Object.assign({
        isValidTill: Date.now() + AttemptService.ATTEMPT_LIFE_TIME_IN_MINUTES * 60 * 1000,
      }
      , lastAttempt ? {
        attemptsCount: +lastAttempt.attemptsCount + 1,
        lastAttemptAt: new Date().getTime(),
        code: lastAttempt.code,
      } : {
        attemptsCount: 1,
        lastAttemptAt: new Date().getTime(),
        code: cryptoRandomString({
          length: AttemptService.ATTEMPT_CODE_LENGTH,
          type: 'numeric',
        }),
      },
    );
  }


  /**
   * Remove user's attempts
   *
   * @param mobileNumber
   */
  public async clearAttempt(mobileNumber: string): Promise<any> {
    return await this.redisClient.del(mobileNumber);
  }
}
