import {
  BadRequestException, Injectable,
  InternalServerErrorException, UnauthorizedException,
}                         from '@nestjs/common';
import CheckCodeDto       from './dto/check-code.dto';
import { UsersService }   from '../users/users.service';
import { JwtService }     from '@nestjs/jwt';
import { SmsService }     from '../sms/sms.service';
import { ConfigService }  from '@nestjs/config';
import SmsType            from '../sms/constants/smsTypes.enum';
import BearerTokenService from './bearerToken.service';
import AttemptService     from './attempt.service';

@Injectable()
export class AuthService {

  /**
   * @param userService
   * @param jwtService
   * @param smsService
   * @param configService
   * @param attemptService
   *
   */
  constructor(
    private readonly userService: UsersService,
    private readonly jwtService: JwtService,
    private readonly smsService: SmsService,
    private readonly configService: ConfigService,
    private readonly attemptService: AttemptService,
  ) {

  }

  /**
   *
   * @param mobileNumber
   */
  public async sendVerificationCode(mobileNumber: string): Promise<{ mobileNumber: string }> {
    const verificationCode = await this.attemptService.storeNewAttempt(mobileNumber);

    // Send the verification code using sms, if it's enabled
    let isSmsSent = true;
    if (this.isEnableToAuthBySms()) {
      isSmsSent = await this.smsService.send(
        SmsType.AUTH_VERIFICATION,
        mobileNumber,
        `Security code for SigmaPay:\n${ verificationCode }`,
      );
    }

    if (!isSmsSent) {
      throw new BadRequestException(
        'There are some problems about sending verification code, please try later',
      );
    }

    return Promise.resolve({ mobileNumber });
  }

  /**
   *
   * @param checkCodeDto
   */
  public async checkVerificationCode(checkCodeDto: CheckCodeDto): Promise<any> {
    const { mobileNumber, verificationCode } = checkCodeDto;

    // Check the verification-code
    await this.checkLastAttempt(mobileNumber, verificationCode);

    // Find or register a user
    const user = await this.findOrRegisterUser(mobileNumber);

    // Generate access-token and refresh-token
    const [accessToken, refreshToken] = await Promise.all([
      new BearerTokenService('ACCESS_TOKEN').generateNewToken(user),
      new BearerTokenService('REFRESH_TOKEN').generateNewToken(user),
    ]);

    // TODO: update the refreshToken
    // await this.userService.updateRefreshToken(user.id, refreshToken);

    // Clear user's attempt
    await this.attemptService.clearAttempt(mobileNumber);

    return Object.assign(user, {
      accessToken,
      refreshToken
    });
  }

  /**
   * Check that we should send verification code by sms or not.
   * Note that there are two values for AUTH_BY_SMS: enable, disable
   *
   */
  private isEnableToAuthBySms() {
    const authBySms = this.configService.get<string>('AUTH_BY_SMS');
    if (authBySms) {
      return authBySms.toLowerCase() === 'enable';
    }
    throw new InternalServerErrorException('AUTH_BY_SMS is not configured correctly!');
  }

  /**
   * Find user by mobileNumber or register new user
   *
   * @param mobileNumber
   */
  private async findOrRegisterUser(mobileNumber: string) {
    try {
      return await this.userService.findByMobileNumber(mobileNumber);
    } catch (e) {
      return await this.userService.registerNewUser({
        mobileNumber,
      });
    }
  }

  /**
   * Check the verificationCode with the last attempt of this mobileNumber
   *
   * @param mobileNumber
   * @param verificationCode
   * @private
   */
  private async checkLastAttempt(mobileNumber: string, verificationCode: string): Promise<void> {
    if (this.isEnableToAuthBySms()) {
      const isCodeValid = await this.attemptService.checkLastAttempt(mobileNumber, verificationCode);
      if (!isCodeValid) {
        throw new UnauthorizedException();
      }
    } else if (verificationCode !== '1234') {
      throw new UnauthorizedException();
    }
  }
}
