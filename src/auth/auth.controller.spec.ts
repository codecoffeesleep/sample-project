import { Test, TestingModule } from '@nestjs/testing';
import { AuthController }      from './auth.controller';
import { AuthService }         from './auth.service';
import SendCodeDto             from './dto/send-code.dto';

describe('Auth Controller', () => {
  let controller: AuthController;
  let service: AuthService;
  let expectedServiceResult: any;
  let requestDto: SendCodeDto;

  class ApiServiceMock {
    sendVerificationCode() {
      return {
        'mobileNumber': '00905538458470',
        'ttl': 1592342978917,
      };
    }
  }

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        { provide: AuthService, useClass: ApiServiceMock },
      ],
    }).compile();

    controller = module.get<AuthController>(AuthController);
    service = module.get<AuthService>(AuthService);

    expectedServiceResult = {
      'mobileNumber': '00905538458470',
      'ttl': 1592342978917,
    };
    requestDto = {
      'countryCode': '0090',
      'mobileNumber': '5538458470',
    };
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('Send code', () => {
    /**
     *
     */
    it('Service should not called before handler execution', async () => {
      jest.spyOn(service, 'sendVerificationCode').mockResolvedValue(expectedServiceResult);
      expect(service.sendVerificationCode).not.toHaveBeenCalled();
    });
    /**
     *
     */
    it('Service should has been called', async () => {
      jest.spyOn(service, 'sendVerificationCode').mockResolvedValue(expectedServiceResult);
      await controller.sendCode(requestDto);
      expect(service.sendVerificationCode).toHaveBeenCalled();
    });
    /**
     *
     */
    it('Check controller response', async () => {
      jest.spyOn(service, 'sendVerificationCode').mockResolvedValue(expectedServiceResult);
      expect(await controller.sendCode(requestDto)).toBe(expectedServiceResult);
    });
  });
});
