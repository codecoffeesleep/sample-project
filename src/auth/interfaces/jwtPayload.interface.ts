export default interface JwtPayloadInterface {
  id: number
  firstName?: string
  lastName?: string
  username?: string
  merchants?: any[]
}
