import { IsNotEmpty, IsString, Matches } from 'class-validator';

export default class SendCodeDto {
  @IsString()
  @IsNotEmpty()
  @Matches(/[+0-9]{2,5}/)
  countryCode: string;

  @IsString()
  @IsNotEmpty()
  mobileNumber: string;
}
