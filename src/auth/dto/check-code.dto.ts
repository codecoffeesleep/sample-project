import { IsNotEmpty, IsOptional, IsString, Matches } from 'class-validator';

export default class CheckCodeDto {
  @IsString()
  @IsNotEmpty()
  @Matches(/[0-9]{4}/)
  verificationCode: string;

  @IsString()
  @IsNotEmpty()
  mobileNumber: string;


  // TODO: ENUM --> MERCHANT, COMPANY
  @IsString()
  @IsOptional()
  scope: string;

}
