import { JwtService }    from '@nestjs/jwt';
import { configService } from '../config.service';

export default class BearerTokenService {

  private readonly jwtService: JwtService;
  private readonly expiresIn: string;

  /**
   *
   * @param type
   */
  constructor(private readonly type: 'ACCESS_TOKEN' | 'REFRESH_TOKEN') {
    let secret = '';
    if (type === 'ACCESS_TOKEN') {
      secret = configService.get<string>('ACCESS_TOKEN_SECRET_KEY');
      this.expiresIn = configService.get<string>('ACCESS_TOKEN_EXPIRES_IN');
    } else {
      secret = configService.get<string>('REFRESH_TOKEN_SECRET_KEY');
      this.expiresIn = configService.get<string>('REFRESH_TOKEN_EXPIRES_IN');
    }

    this.jwtService = new JwtService({ secret });
  }

  /**
   * Generate new token
   *
   */
  public async generateNewToken(payload: object): Promise<string> {
    let token = '';

    if (this.type === 'ACCESS_TOKEN') {
      token = await this.generateAccessToken(payload);
    } else {
      token = await this.generateRefreshToken(payload);
    }

    return Promise.resolve(token);
  }

  /**
   * Check the token is valid or not
   *
   */
  public async verify(token): Promise<boolean> {
    try {
      this.jwtService.verify(token);
      return true;
    } catch (e) {
      return false;
    }
  }

  /**
   * Generate a new access token
   *
   */
  private async generateAccessToken(payload: object): Promise<string> {
    return this.jwtService.sign({
      id: payload['id'],
      firstName: payload['firstName'],
      lastName: payload['lastName'],
      username: payload['username'],
    },{
      expiresIn: this.expiresIn
    });
  }

  /**
   * Generate a new refresh token
   *
   */
  private async generateRefreshToken(payload: object): Promise<string> {
    return this.jwtService.sign({
      id: payload['id'],
    });
  }
}
