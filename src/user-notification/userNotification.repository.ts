import { EntityRepository, Repository } from 'typeorm';
import { UserNotification }             from './userNotification.entity';

@EntityRepository(UserNotification)
export default class UserNotificationRepository extends Repository<UserNotification> {

}
