enum UserNotificationStatuses {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE'
}

export default UserNotificationStatuses;
