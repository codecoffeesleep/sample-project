import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne, JoinColumn,
}                               from 'typeorm';
import { User }                 from '../users/user.entity';
import UserNotificationStatuses from './constants/userNotificationStatuses.enum';

@Entity({
  name: 'user_notifications',
})

export class UserNotification {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'user_id',
  })
  userId: number;

  @Column({
    name: 'player_id',
  })
  playerId: string;

  @Column({
    name: 'status',
  })
  status: UserNotificationStatuses;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @ManyToOne(type => User, user => user.userNotifications)
  @JoinColumn({ name: 'user_id' })
  user: User;

}
