enum MerchantRole {
  OWNER = 'OWNER',
  CASHIER = 'CASHIER'
}

export default MerchantRole
