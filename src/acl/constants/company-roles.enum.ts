enum CompanyRole {
  OWNER = 'OWNER',
  CASHIER = 'CASHIER'
}

export default CompanyRole
