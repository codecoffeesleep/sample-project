import { Entity, Column, ManyToOne, PrimaryGeneratedColumn, BaseEntity, JoinColumn } from 'typeorm';
import { Merchant }                                                                  from '../../merchants/merchant.entity';
import { User }                                                                      from '../../users/user.entity';
import MerchantRole
                                                                                     from '../constants/merchant-roles.enum';

@Entity({
  name: 'merchant_user',
})
export class MerchantToUser {

  @PrimaryGeneratedColumn()
  public id!: number;

  @Column({
    name: 'merchant_id',
  })
  public merchantId: number;

  @Column({
    name: 'user_id',
  })
  public userId: number;

  @Column()
  public role: MerchantRole;

  // @ts-ignore
  @ManyToOne(type => Merchant, merchant => merchant.users)
  @JoinColumn({ name: 'merchant_id' })
  public merchant: Merchant;

  // @ts-ignore
  @ManyToOne(type => User, user => user.merchants)
  @JoinColumn({ name: 'user_id' })
  public user: User;
}
