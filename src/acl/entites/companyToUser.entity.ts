import { Entity, Column, ManyToOne, PrimaryGeneratedColumn, JoinColumn, BaseEntity } from 'typeorm';
import { Company }                                                                   from "../../companies/company.entity";
import { User }                                                                      from "../../users/user.entity";
import CompanyRole                                                                   from '../constants/company-roles.enum';

@Entity({
  name: 'company_user'
})
export class CompanyToUser {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'company_id'
  })
  companyId!: number;

  @Column({
    name: 'user_id',
  })
  public userId!: number;

  @Column()
  public role!: CompanyRole;

  @ManyToOne(type => User, user => user.userCompanies)
  @JoinColumn({ name: 'user_id' })
  user: User;

  @ManyToOne(type => Company, company => company.userCompanies)
  @JoinColumn({ name: 'company_id' })
  company: Company;
}
