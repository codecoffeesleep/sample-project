import { Injectable, CanActivate, ExecutionContext, ForbiddenException } from '@nestjs/common';
import { Reflector }                                                     from '@nestjs/core';
import { AclService }                                                    from '../acl.service';
import MerchantRole                                                      from '../constants/merchant-roles.enum';
import { Merchant }                                                      from '../../merchants/merchant.entity';
import { Company }                                                       from '../../companies/company.entity';
import CompanyRole                                                       from '../constants/company-roles.enum';

@Injectable()
export class CheckPermissionGuard implements CanActivate {
  constructor(
    private reflector: Reflector,
    private readonly aclService: AclService,
  ) {
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const aclConditions = this.reflector.get<{
      resource: any,
      roles: string[],
    }>('acl', context.getHandler());

    if (!aclConditions) {
      return true;
    }

    const request = context.switchToHttp().getRequest();
    const user = request.user;
    const resourceId = request.params['resource_id'] || null;

    switch (aclConditions.resource.name) {
      case Company.name:
        await this.aclService.checkCompanyPermission(
          user.id,
          resourceId,
          aclConditions.roles as CompanyRole[],
        );
        break;
      case Merchant.name:
        await this.aclService.checkMerchantPermission(
          user.id,
          resourceId,
          aclConditions.roles as MerchantRole[],
        );
        break;
      default:
        throw new ForbiddenException('Resource Not Found');
    }

    return true;
  }
}
