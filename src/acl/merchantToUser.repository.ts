import { EntityRepository, Repository } from 'typeorm';
import { MerchantToUser }               from './entites/merchantToUser.entity';

@EntityRepository(MerchantToUser)
export class MerchantToUserRepository extends Repository<MerchantToUser> {

  /**
   *
   * @param userId
   */
  public async getRelatedMerchants(userId: number): Promise<{
    'id': number,
    'merchant': {
      'id': number,
      'name': string,
      'logo': string
    }
  }[]> {
    return await this.createQueryBuilder('conj')
      .innerJoin('conj.merchant', 'merchant')
      .select([
        'conj.id',
        'merchant.id',
        'merchant.name',
        'merchant.logo',
      ])
      .getMany();
  }

}
