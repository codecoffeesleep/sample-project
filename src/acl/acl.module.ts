import { forwardRef, Module } from '@nestjs/common';
import { AclService } from './acl.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from '../auth/auth.module';
import { CompanyToUserRepository } from './companyToUser.repository';
import { MerchantToUserRepository } from './merchantToUser.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([CompanyToUserRepository, MerchantToUserRepository]),
    // We use this kind of importing because of circular-dependency
    forwardRef(() => AuthModule),
  ],
  providers: [AclService],
  exports: [AclService],
})
export class AclModule {
}
