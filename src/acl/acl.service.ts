import { ForbiddenException, Injectable } from '@nestjs/common';
import { MerchantToUser }                 from './entites/merchantToUser.entity';
import MerchantRole                       from './constants/merchant-roles.enum';
import { In }                             from 'typeorm';
import CompanyRole                        from './constants/company-roles.enum';
import { CompanyToUser }                  from './entites/companyToUser.entity';
import { InjectRepository }               from '@nestjs/typeorm';
import { CompanyToUserRepository }        from './companyToUser.repository';
import { MerchantToUserRepository }       from './merchantToUser.repository';

@Injectable()
export class AclService {

  constructor(
    @InjectRepository(CompanyToUserRepository)
    private readonly companyToUserRepository: CompanyToUserRepository,
    @InjectRepository(MerchantToUserRepository)
    private readonly merchantToUserRepository: MerchantToUserRepository,
  ) {
  }

  /**
   *
   * @param userId
   * @param merchantId
   * @param roles
   */
  public async checkMerchantPermission(userId: number, merchantId: number, roles: MerchantRole[]): Promise<MerchantToUser> {
    const conditions: any = { userId, merchantId, role: In(roles) };
    const isExist = await this.merchantToUserRepository.findOne(conditions);

    if (isExist) {
      return Promise.resolve(isExist);
    }

    throw new ForbiddenException('You do not have access to this content!');
  }

  /**
   *
   * @param userId
   * @param companyId
   * @param roles
   */
  public async checkCompanyPermission(userId: number, companyId: number, roles: CompanyRole[]): Promise<CompanyToUser> {
    const conditions: any = { userId, companyId, role: In(roles) };

    const isExist = await this.companyToUserRepository.findOne(conditions);
    if (isExist) {
      return Promise.resolve(isExist);
    }

    throw new ForbiddenException('You do not have access to this content!');
  }

}
