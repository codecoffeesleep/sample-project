import { EntityRepository, Repository } from 'typeorm';
import { CompanyToUser }                         from  './entites/companyToUser.entity';

@EntityRepository(CompanyToUser)
export class CompanyToUserRepository extends Repository<CompanyToUser> {

}
