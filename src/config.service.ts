
import { TypeOrmModuleOptions }         from '@nestjs/typeorm';
import { join }                         from 'path';
import { ConfigService }                from '@nestjs/config';
import { InternalServerErrorException } from '@nestjs/common';

require('dotenv').config();

class AppConfig extends ConfigService {
  /**
   *
   * @param env
   */
  constructor(private env: { [k: string]: string | undefined }) {
    super();
  }

  /**
   *
   * @param key
   * @param throwOnMissing
   */
  private getValue(key: string, throwOnMissing: boolean = true): any {
    const value = this.get(key);
    if (!value && throwOnMissing) {
      throw new InternalServerErrorException(`config error - missing env.${ key }`);
    }
    return value;
  }

  /**
   *
   * @param keys
   */
  public ensureValues(keys: string[]): AppConfig {
    keys.forEach(k => this.getValue(k, true));
    return this;
  }

  /**
   *
   */
  public getTypeOrmConfig(): TypeOrmModuleOptions {
    return {
      type: 'postgres',
      host: this.getValue('DATABASE_HOST'),
      port: parseInt(this.getValue('DATABASE_PORT')),
      username: this.getValue('DATABASE_USER'),
      password: this.getValue('DATABASE_PASSWORD'),
      database: this.getValue('DATABASE_DB'),
      entities: [join(__dirname, '**', '*.entity.{ts,js}')],
      synchronize: false,
      migrationsTableName: 'migrations',
      migrations: [join(__dirname, '../database', 'migration', '*.ts')],
      cli: {
        migrationsDir: './database/migration',
      },
      logging: process.env.ENVIRONMENT != 'PRODUCTION'
    };
  }
}

const configService = new AppConfig(process.env)
  .ensureValues([
    'DATABASE_HOST',
    'DATABASE_DB',
    'DATABASE_USER',
    'DATABASE_PASSWORD',
  ]);

export { configService };
