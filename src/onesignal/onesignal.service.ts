import { Injectable }             from '@nestjs/common';
import * as  unirest              from 'unirest';
import { ConfigService }          from '@nestjs/config';
import UserNotificationRepository from '../user-notification/userNotification.repository';
import UserNotificationStatuses   from '../user-notification/constants/userNotificationStatuses.enum';

@Injectable()
export class OnesignalService {

  /**
   * Onesignal App ID
   */
  public appId;

  /**
   * Onesignal REST API-Key for sending notification
   */
  public APIKey;

  /**
   * Onesignal base URL sending notification
   */
  public baseURL;

  /**
   *
   * @param configService
   */
  constructor(
    private readonly configService: ConfigService,
    private readonly userNotificationRepository: UserNotificationRepository
  ) {
    this.appId = this.configService.get<string>('ONESIGNAL_APP_ID');
    this.APIKey = this.configService.get<string>('ONESIGNAL_APP_KEY');
    this.baseURL = this.configService.get<string>('ONESIGNAL_BASE_URL');
  }

  /**
   *
   * @param userId
   * @param data
   */
  public async send(userId, data) {
    const userNotifications = await this.userNotificationRepository.find({ userId, status: UserNotificationStatuses.ACTIVE });
    if(userNotifications && userNotifications.length) {
      const onesignalRequest = unirest('POST', this.baseURL);
      const playerIds = []
      onesignalRequest.headers({
        'Content-Type': `application/json; charset=utf-8`,
        'Authorization': `Basic ${this.APIKey}`
      });
      userNotifications.forEach((userNotification) => {
        playerIds.push(userNotification.playerId)
      })
      onesignalRequest.send({
        app_id: this.appId,
        headings: data.title,
        contents: data.body,
        app_url: data.app_url,
        content_available: true,
        include_player_ids: playerIds,
        priority: 10
      });
    }
  }
}
