import { Module, Global } from '@nestjs/common';
import { OnesignalService } from './onesignal.service';
import { ConfigModule }  from '@nestjs/config';

@Global()
@Module({
  imports: [ConfigModule],
  providers: [OnesignalService]
})
export class OnesignalModule {}
