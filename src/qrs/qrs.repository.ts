import { EntityRepository, Repository } from 'typeorm';
import { QR }                           from './qr.entity';
import { NotFoundException }            from '@nestjs/common';

@EntityRepository(QR)
export class QrsRepository extends Repository<QR> {
  public async findByCode(code: string): Promise<QR> {
    const qr = await this.createQueryBuilder('qr')
      .where('code = :code', { code })
      .innerJoin('qr.merchant', 'merchant')
      .select([
        'qr.id',
        'qr.defaultAmount',
        'qr.version',
        'qr.code',
        'qr.type',
        'merchant.id',
        'merchant.name',
        'merchant.logo',
        'merchant.address',
      ])
      .getOne();

    if (qr) {
      return qr;
    }

    throw new NotFoundException('QR not found');
  }
}
