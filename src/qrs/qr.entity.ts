import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne, OneToMany, JoinColumn,
}                      from 'typeorm';
import { Merchant }    from '../merchants/merchant.entity';
import { Transaction } from '../transactions/transaction.entity';
import QRType          from './constants/qrType.enum';

@Entity({
  name: 'qrs',
})

export class QR {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'merchant_id',
  })
  merchantId: number;

  @Column({
    name: 'default_amount',
  })
  defaultAmount: number;

  @Column({
    name: 'code',
  })
  code: string;

  @Column({
    name: 'version',
  })
  version: string;

  @Column({
    name: 'type',
    nullable: false,
  })
  type: QRType;

  @Column({
    name: 'expired_at',
    nullable: true,
  })
  expireTime: Date;

  @CreateDateColumn({
    name: 'created_at',
    nullable: true,
  })
  createdAt: Date;

  @UpdateDateColumn({
    name: 'updated_at',
    nullable: true,
  })
  updatedAt: Date;

  @ManyToOne(type => Merchant, merchant => merchant.qrs)
  @JoinColumn({ name: 'merchant_id' })
  merchant: Merchant;

  @OneToMany(type => Transaction, transaction => transaction.qr)
  transactions: Transaction[];

}
