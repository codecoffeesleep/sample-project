import { BadRequestException, Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectRepository }                                              from '@nestjs/typeorm';
import { QrsRepository }                                                 from './qrs.repository';
import QRType                                                            from './constants/qrType.enum';
import { TransactionsService }                                           from '../transactions/transactions.service';
import { WalletsService }                                                from '../wallets/wallets.service';
import TransactionType
                                                                         from '../transactions/constants/transactionTypes.enum';
import { QR }                                                            from './qr.entity';
import WalletStatuses
                                                                         from '../wallets/constants/walletStatuses.enum';

@Injectable()
export class QrsService {
  /**
   *
   * @param qrRepository
   * @param transactionService
   * @param walletsService
   */
  constructor(
    @InjectRepository(QrsRepository)
    private readonly qrRepository: QrsRepository,
    private readonly transactionService: TransactionsService,
    private readonly walletsService: WalletsService,
  ) {
  }

  /**
   * Find a qr by code
   *
   * @param code
   */
  public async findByCode(code: string): Promise<QR> {
    // expireTime: MoreThanOrEqual(new Date())
    return await this.qrRepository.findByCode(code);
  }

  /**
   * Pay a QR
   * TODO: we should do this transactional
   *
   * @param userId
   * @param qrCode
   * @param walletId
   * @param amount
   */
  public async pay(
    userId: number,
    qrCode: string,
    walletId: number,
    amount?: number,
  ): Promise<any> {
    // 1.Find amount
    const qr = await this.findByCode(qrCode);

    if (qr.type === QRType.FIXED || !amount) {
      amount = qr.defaultAmount;
    }

    // 2.Find source
    const sourceWallet = await this.walletsService.getUserSingleWallet(userId, walletId);
    if (sourceWallet.status !== WalletStatuses.ACTIVE) {
      throw new BadRequestException('Your wallet is not active');
    }

    // 3.Find destination
    const merchantId = qr.merchant.id;
    const merchantWallets = await this.walletsService.getMerchantWallets(
      merchantId,
    );

    if (!merchantWallets.length) {
      throw new InternalServerErrorException(
        'Merchant has not any active wallet',
      );
    }
    const destinationWallet = merchantWallets[0];

    // 4.Store a new transaction
    const transaction = await this.transactionService.transfer(
      sourceWallet,
      destinationWallet,
      amount,
      TransactionType.QR,
      qr,
    );

    // 5.Update wallets
    if (transaction) {
      await this.walletsService.decreaseBalance(sourceWallet.id, amount);
      await this.walletsService.increaseBalance(destinationWallet.id, amount);
    }

    // 6. Decorate appropriate transaction object
    return {
      id: transaction.id,
      amount: transaction.amount,
      type: transaction.type,
      createdAt: transaction.createdAt,
      merchant: {
        name: transaction.destinationWallet.merchant.name,
        logo: transaction.destinationWallet.merchant.logo,
      },
    };
  }
}
