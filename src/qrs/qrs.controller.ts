import { Body, Controller, Get, Param, Post, Req, UseGuards } from '@nestjs/common';
import { QrsService }                                         from './qrs.service';
import { AuthGuard }                                          from '@nestjs/passport';
import { ControllerResponse }                                 from '../common/response-decorator/responses.interface';

@Controller('qr')
@UseGuards(AuthGuard())
export class QrsController {

  constructor(private readonly qrService: QrsService) {
  }

  /**
   * Fetch the data of a QR
   *
   * @param code
   */
  @Get('/:code')
  public async single(@Param('code') code: string): Promise<ControllerResponse> {
    const qrInfo = await this.qrService.findByCode(code);
    return {
      data: qrInfo,
    };
  }

  /**
   * Pay a QR
   *
   * @param body
   * @param code
   * @param request
   */
  @Post('/:code')
  public async pay(@Body() body, @Param('code') code, @Req() request): Promise<ControllerResponse> {
    const { user } = request;
    const { amount, wallet } = body;
    const transaction = await this.qrService.pay(user.id, code, wallet, amount);
    return {
      data: transaction,
    };
  }
}
