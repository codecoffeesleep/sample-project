import { Module }             from '@nestjs/common';
import { QrsService }         from './qrs.service';
import { QrsController }      from './qrs.controller';
import { TypeOrmModule }      from '@nestjs/typeorm';
import { QrsRepository }      from './qrs.repository';
import { AuthModule }         from '../auth/auth.module';
import { TransactionsModule } from '../transactions/transactions.module';
import { WalletsModule }      from '../wallets/wallets.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([QrsRepository]),
    AuthModule,
    TransactionsModule,
    WalletsModule
  ],
  providers: [QrsService],
  controllers: [QrsController]
})
export class QrsModule {}
