enum QRType {
  STATIC = 'STATIC',
  FIXED = 'FIXED_PRICE',
}

export default QRType;
