import { EntityRepository, Repository } from 'typeorm';
import SMS                              from './sms.entity';

@EntityRepository(SMS)
export default class SmsRepository extends Repository<SMS> {

}
