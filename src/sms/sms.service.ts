import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { TwilioGateway }                            from './gateways/twilio.gateway';
import { ConfigService }                            from '@nestjs/config';
import { InjectRepository }                         from '@nestjs/typeorm';
import SmsRepository                                from './sms.repository';
import SMS                                          from './sms.entity';
import SmsProvider                                  from './constants/smsProviders.enum';
import SmsType                                      from './constants/smsTypes.enum';

@Injectable()
export class SmsService {

  private readonly provider: string;

  /**
   *
   * @param smsRepository
   * @param configService
   */
  constructor(
    @InjectRepository(SmsRepository)
    private readonly smsRepository: SmsRepository,
    private readonly configService: ConfigService,
  ) {
    this.provider = configService.get<string>('SMS_PROVIDER').toUpperCase();
  }

  /**
   *
   * @param type: SmsType
   * @param mobileNumber: string
   * @param body: string
   */
  public async send(type: SmsType, mobileNumber: string, body: string): Promise<boolean> {
    let sid = null;

    if (this.provider === SmsProvider.TWILIO) {
      sid = await new TwilioGateway().send(mobileNumber, body);
    } else {
      throw new InternalServerErrorException('Sms provider is not configured correctly!');
    }

    if (sid) {
      this.store(type, mobileNumber, body, sid);
      return true;
    }

    return false;
  }

  /**
   * Store new sms
   *
   * @param type: SmsType
   * @param mobileNumber: sid
   * @param body: sid
   * @param sid:: sid
   */
  private async store(
    type: SmsType,
    mobileNumber: string,
    body: string,
    sid: string,
  ): Promise<void> {
    const sms = new SMS();

    sms.provider = this.provider;
    sms.type = type;
    sms.mobileNumber = mobileNumber;
    sms.body = body;
    sms.sid = sid;

    await this.smsRepository.save(sms);
  }

}
