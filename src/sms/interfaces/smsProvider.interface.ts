export interface SmsProviderInterface {
  send(to: string, body: string): Promise<string | null>
}
