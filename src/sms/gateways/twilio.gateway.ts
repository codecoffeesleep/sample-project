import { SmsProviderInterface }         from '../interfaces/smsProvider.interface';
import { ConfigService }                from '@nestjs/config';
import { InternalServerErrorException } from '@nestjs/common';

export class TwilioGateway implements SmsProviderInterface {
  private readonly twilioClient: any;
  private static PHONE_NUMBER: string;

  /**
   * Create a new client for connecting to twilio
   *
   */
  constructor() {
    const configService = new ConfigService();
    TwilioGateway.PHONE_NUMBER = configService.get<string>('TWILIO_FROM');

    try {
      this.twilioClient = require('twilio')(
        configService.get<string>('TWILIO_SID'),
        configService.get<string>('TWILIO_AUTH_TOKEN'),
      );
    } catch (e) {
      throw new InternalServerErrorException('Twilio sid or auth_token is wrong!');
    }
  }

  /**
   * Send an sms using twilio
   *
   * @param to: string
   * @param body: string
   */
  async send(to: string, body: string): Promise<string | null> {
    try {
      const twilioResponse = await this.twilioClient.messages
        .create({
          from: TwilioGateway.PHONE_NUMBER,
          body,
          to,
        });
      return twilioResponse.sid || null;
    } catch (e) {
      throw new InternalServerErrorException('Could not connect to twilio');
    }
  }
}
