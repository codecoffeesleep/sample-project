import { Module }        from '@nestjs/common';
import { SmsService }    from './sms.service';
import { ConfigModule }  from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import SmsRepository     from './sms.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([SmsRepository]),
    ConfigModule
  ],
  providers: [SmsService],
  exports: [SmsService],
})
export class SmsModule {
}
