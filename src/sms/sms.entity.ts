import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import SmsProvider
                                                                                      from './constants/smsProviders.enum';
import SmsType                                                                        from './constants/smsTypes.enum';

@Entity('sms')
export default class SMS {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'mobile_number',
  })
  mobileNumber: string;

  @Column({
    name: 'body'
  })
  body: string;

  @Column({
    name: 'provider',
    enum: SmsProvider,
  })
  provider: string;

  @Column({
    name: 'type',
    enum: SmsType,
  })
  type: string;

  @Column({
    name: 'sid',
    nullable: true,
  })
  sid: string;

  @CreateDateColumn({
    name: 'created_at',
  })
  createdAt: Date;

  @UpdateDateColumn({
    name: 'updated_at',
  })
  updatedAt: Date;
}
