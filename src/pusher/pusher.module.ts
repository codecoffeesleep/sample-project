import { Global, Module } from '@nestjs/common';
import { PusherService }  from './pusher.service';
import { ConfigModule }   from '@nestjs/config';

@Global()
@Module({
  imports: [ConfigModule],
  providers: [PusherService]
})
export class PusherModule {}
