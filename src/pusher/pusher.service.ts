import { Injectable }         from '@nestjs/common';
import { ConfigService }      from '@nestjs/config';
import * as Pusher            from 'pusher';

@Injectable()
export class PusherService {

  /**
   *
   */
  public instance;

  /**
   *
   * @param configService
   */
  constructor(private readonly configService: ConfigService) {
    this.instance = new Pusher({
      appId: this.configService.get<string>('PUSHER_APP_ID'),
      key: this.configService.get<string>('PUSHER_APP_KEY'),
      secret: this.configService.get<string>('PUSHER_SECRET_KEY'),
      cluster: 'eu',
      useTLS: true
    });
  }

  /**
   *
   * @param to
   * @param type
   * @param data
   */
  public async send(to, type, data) {
    this.instance.trigger(to, type, data);
    return;
  }

}
