import { Global, Module }  from '@nestjs/common';
import { BranchioService } from './branchio.service';
import { ConfigModule }    from '@nestjs/config';

@Global()
@Module({
  imports: [ConfigModule],
  providers: [BranchioService],
})
export class BranchioModule {
}
