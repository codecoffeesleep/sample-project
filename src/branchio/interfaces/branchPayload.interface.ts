export default interface BranchPayloadInterface {
  alias: string
  channel?: string
  feature?: string
  campain?: string
  tags?: string[]
  data: DataInterface
}

interface DataInterface {
  custom_bool: boolean
  $og_title: string
  $og_description: string
  $og_image_url: string
}
