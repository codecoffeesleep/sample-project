import { Test, TestingModule } from '@nestjs/testing';
import { BranchioService } from './branchio.service';

describe('BranchioService', () => {
  let service: BranchioService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BranchioService],
    }).compile();

    service = module.get<BranchioService>(BranchioService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
