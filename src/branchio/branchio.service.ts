import { Injectable }         from '@nestjs/common';
import * as  branchio         from 'branchio-sdk';
import BranchPayloadInterface from './interfaces/branchPayload.interface';
import { ConfigService }      from '@nestjs/config';

@Injectable()
export class BranchioService {

  /**
   *
   */
  public instance;

  /**
   *
   * @param configService
   */
  constructor(private readonly configService: ConfigService) {
    this.instance = branchio({
      appId: this.configService.get<string>('BRANCH_APP_ID'),
      key: this.configService.get<string>('BRANCH_IO_KEY'),
      secret: this.configService.get<string>('BRANCH_IO_SECRET'),
    });
  }

  /**
   *
   * @param data
   */
  public async getLink(data: BranchPayloadInterface) {
    const { url } = await this.instance.link(data);
    return url;
  }

}
