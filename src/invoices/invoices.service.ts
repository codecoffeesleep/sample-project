import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository }              from '@nestjs/typeorm';
import { InvoicesRepository }            from './invoices.repository';
import InvoiceTypes                      from './constants/invoiceTypes.enum';

@Injectable()
export class InvoicesService {

  constructor(
    @InjectRepository(InvoicesRepository)
    private readonly invoicesRepository: InvoicesRepository
  ) {
  }

  /**
   *
   * @param walletId
   */
  public async getAllSettlementsByWallet(walletId: number) {
    return await this.invoicesRepository.find({
      where: {
        walletId,
        type: InvoiceTypes.SETTLEMENT
      },
      relations: ['transactions', 'transactions.sourceWallet', 'transactions.destinationWallet']
    })
  }

  /**
   *
   * @param walletId
   * @param id
   */
  public async getSingleSettlementsByIdAndWallet(walletId: number, id: number) {
    const settlement = await this.invoicesRepository.findOne({
        id,
        walletId,
        type: InvoiceTypes.SETTLEMENT
      },
      { relations: ['transactions', 'transactions.sourceWallet', 'transactions.destinationWallet'] })
    if (settlement) {
      return settlement
    } else {
      throw new NotFoundException('Settlement not found');
    }
  }
}
