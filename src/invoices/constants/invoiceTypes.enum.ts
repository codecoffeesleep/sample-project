enum InvoiceTypes {
  SETTLEMENT = 'SETTLEMENT',
  COMPANY_PAYMANT = 'COMPANY_PAYMANT'
}

export default InvoiceTypes;
