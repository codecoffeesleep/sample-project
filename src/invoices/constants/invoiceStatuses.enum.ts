enum InvoiceStatuses {
  DONE = 'DONE',
  PEND = 'PEND',
  FAILED = 'FAILED',
}

export default InvoiceStatuses
