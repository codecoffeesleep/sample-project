import { Module }             from '@nestjs/common';
import { InvoicesService }    from './invoices.service';
import { TypeOrmModule }      from '@nestjs/typeorm';
import { InvoicesRepository } from './invoices.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([InvoicesRepository])
  ],
  providers: [InvoicesService],
  exports: [InvoicesService]
})
export class InvoicesModule {}
