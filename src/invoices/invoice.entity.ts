import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne, OneToMany, JoinColumn,
}                      from 'typeorm';
import { Wallet }      from '../wallets/wallet.entity';
import { Transaction } from '../transactions/transaction.entity';
import InvoiceStatuses from './constants/invoiceStatuses.enum';
import InvoiceTypes    from './constants/invoiceTypes.enum';

@Entity({
  name: 'invoices',
})
export class Invoice {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'amount',
  })
  amount: number;

  @Column({
    name: 'wallet_id',
  })
  walletId: number;

  @Column({
    name: 'status',
  })
  status: InvoiceStatuses;

  @Column({
    name: 'type',
  })
  type: InvoiceTypes;

  @Column({
    name: 'transaction_number',
  })
  transactionNumber: number;

  @Column({
    name: 'from_date',
    nullable: true
  })
  fromDate: Date

  @Column({
    name: 'to_date',
    nullable: true
  })
  toDate: Date

  @Column({
    type: 'jsonb',
    array: false,
    nullable: true,
  })
  description: object

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @ManyToOne(type => Wallet, wallet => wallet.invoices)
  @JoinColumn({ name: 'wallet_id' })
  wallet: Wallet;

  @OneToMany(type => Transaction, transaction => transaction.invoice)
  transactions: Transaction[];
}
