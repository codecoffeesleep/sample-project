import { EntityRepository }  from 'typeorm';
import { Wallet }            from './wallet.entity';
import BaseRepository        from '../common/repositories/base.repository';
import { NotFoundException } from '@nestjs/common';

@EntityRepository(Wallet)
export class WalletsRepository extends BaseRepository<Wallet> {

  private static POPULAR_FIELDS = [
    'wallet.id',
    'wallet.userId',
    'wallet.type',
    'wallet.balance',
    'wallet.status',
    'wallet.title',
    'wallet.employeeNo',
    'wallet.department',
    'wallet.createdAt',
    'wallet.updatedAt',
    'company.id',
    'company.name',
    'merchant.id',
    'merchant.name',
  ];

  public async findUsersWallet(filter, page, limit): Promise<[Wallet[], number]> {
    return await this.createQueryBuilder('wallet')
      .leftJoinAndSelect('wallet.company', 'company')
      .leftJoinAndSelect('wallet.merchant', 'merchant')
      .where('user_id = :userId', { userId: filter.userId })
      .select(WalletsRepository.POPULAR_FIELDS)
      .take(limit)
      .skip((page - 1) * limit)
      .getManyAndCount();
  }

  public async findById(walletId: number, ownerId: number, ownerType: 'company' | 'merchant' | 'personal'): Promise<Wallet> {
    const wallet = await this.createQueryBuilder('wallet')
      .leftJoinAndSelect('wallet.company', 'company')
      .leftJoinAndSelect('wallet.merchant', 'merchant')
      .where('wallet.id = :walletId', { walletId })
      .andWhere('user_id = :ownerId', { ownerId })
      .select(WalletsRepository.POPULAR_FIELDS)
      .getOne();

    if (wallet) {
      return wallet;
    }

    throw new NotFoundException('Wallet not found');
  }

}
