import { Controller, Get, Param, Query, Req, UseGuards } from '@nestjs/common';
import { AuthGuard }                                     from '@nestjs/passport';
import { WalletsService }                                from './wallets.service';
import { Request }                                       from 'express';
import { ControllerResponse }                            from '../common/response-decorator/responses.interface';
import PaginationDto                                     from '../common/dto/pagination.dto';

@Controller('wallets')
@UseGuards(AuthGuard())
export class WalletsController {

  /**
   *
   * @param walletService
   */
  constructor(private readonly walletService: WalletsService) {
  }

  /**
   * Represent all user's wallets
   *
   * @param request
   * @param query
   */
  @Get()
  public async all(@Req() request: Request, @Query() query: PaginationDto): Promise<ControllerResponse> {
    const user = request['user'];
    const { page, limit, filter } = query;
    const result = await this.walletService.getPaginatedUserWallets(user.id, filter, +page, +limit);
    return {
      data: result.wallets,
      total: result.total,
    };
  }

  /**
   * Get user's single wallet
   *
   * @param request
   * @param id
   */
  @Get('/:id')
  public async single(@Req() request: Request, @Param('id') id) {
    const user = request['user'];
    const wallet = await this.walletService.getUserSingleWallet(user.id, id);
    return {
      data: wallet,
    };
  }
}
