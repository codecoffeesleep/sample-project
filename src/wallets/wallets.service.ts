import {
  BadRequestException,
  Injectable,
  NotFoundException,
  OnModuleInit,
}                              from '@nestjs/common';
import { InjectRepository }    from '@nestjs/typeorm';
import { WalletsRepository }   from './wallets.repository';
import { Wallet }              from './wallet.entity';
import WalletStatuses
                               from './constants/walletStatuses.enum';
import { In, getRepository }   from 'typeorm';
import { TransactionsService } from 'src/transactions/transactions.service';
import { Transaction }         from 'src/transactions/transaction.entity';
import { InvoicesService }     from 'src/invoices/invoices.service';
import { Invoice }             from 'src/invoices/invoice.entity';
import { UsersService }        from 'src/users/users.service';
import WalletTypes
                               from './constants/walletTypes.enum';
import { ModuleRef }           from '@nestjs/core';

@Injectable()
export class WalletsService implements OnModuleInit {

  private usersService: UsersService;
  private transactionsService: TransactionsService;

  /**
   *
   * @param walletRepository
   * @param invoiceService
   * @param moduleRef
   */
  constructor(
    @InjectRepository(WalletsRepository)
    private readonly walletRepository: WalletsRepository,
    private readonly invoiceService: InvoicesService,
    private readonly moduleRef: ModuleRef,
  ) {
  }

  /**
   * Load circular dependency modules
   *
   */
  onModuleInit(): any {
    this.usersService = this.moduleRef.get(UsersService, { strict: false });
    this.transactionsService = this.moduleRef.get(TransactionsService, { strict: false });
  }

  /**
   * Get user's wallets
   *
   * @param userId
   * @param filter
   * @param page
   * @param limit
   */
  public async getPaginatedUserWallets(userId: number, filter: any, page: number, limit: number): Promise<{ wallets: Wallet[], total: number }> {
    const [wallets, total] = await this.walletRepository.findUsersWallet({ userId, ...filter }, page, limit);
    return { wallets, total };
  }

  /**
   * Get all user's wallets
   *
   * @param userId
   */
  public async getAllUserWallets(userId: number): Promise<Wallet[]> {
    return await this.walletRepository.find({ userId });
  }


  /**
   * Get user's single wallet
   *
   * @param userId
   * @param walletId
   */
  public async getUserSingleWallet(userId: number, walletId: number): Promise<Wallet> {
    return await this.walletRepository.findById(walletId, userId, 'merchant');
  }

  /**
   *
   * @param companyId
   */
  public async getCompanyWallets(companyId: number): Promise<Wallet[]> {
    return await this.walletRepository.find({
      companyId,
      userId: null,
      status: WalletStatuses.ACTIVE,
    });
  }

  /**
   *
   * @param companyId
   * @param findOptions
   */
  public async getCompanyEmployeeWallets(companyId: number, findOptions: any): Promise<Wallet[]> {
    const { startDate, endDate, maxBalance, minBalance } = findOptions;
    const query = getRepository(Wallet).createQueryBuilder('wallet')
      .leftJoinAndSelect('wallet.user', 'user')
      .andWhere('wallet.userId IS NOT NULL')
      .andWhere('wallet.companyId = :companyId', { companyId })
      .andWhere('wallet.status = :status', { status: WalletStatuses.ACTIVE })
      .andWhere('wallet.type = :type', { type: WalletTypes.COMPANY });

    if (startDate) {
      query.andWhere('wallet.createdAt >= :startDate', { startDate });
    }
    if (endDate) {
      query.andWhere('wallet.createdAt <= :endDate', { endDate });
    }
    if (maxBalance) {
      query.andWhere('wallet.balance <= :maxBalance', { maxBalance });
    }
    if (minBalance) {
      query.andWhere('wallet.balance >= :minBalance', { minBalance });
    }

    return await query.getMany();
  }

  /**
   * Add a wallet for a user.
   * This function is called when user was registered.
   *
   * @param userId
   */
  public async addPersonalWallet(userId: number) {
    const newWallet = new Wallet();
    newWallet.userId = userId;
    newWallet.type = WalletTypes.PERSONAL;
    newWallet.status = WalletStatuses.ACTIVE;
    return await this.walletRepository.save(newWallet);
  }

  /**
   *
   * @param companyId
   * @param employeeData
   */
  public async addCompanyEmployeeWallet(companyId: number, employeeData: any) {
    let employeeUser = await this.usersService.findByMobileNumber(employeeData.mobileNumber, false);
    if (!employeeUser) {
      employeeUser = await this.usersService.registerNewUser(employeeData);
    }
    const wallet = await this.walletRepository.findOne({
      companyId,
      userId: employeeUser.id,
      status: WalletStatuses.ACTIVE,
    });
    if (!wallet) {
      const newWallet = new Wallet();
      Object.assign(newWallet, employeeData);
      newWallet.userId = employeeUser.id;
      newWallet.companyId = companyId;
      newWallet.type = WalletTypes.COMPANY;
      newWallet.status = WalletStatuses.ACTIVE;
      return await this.walletRepository.save(newWallet);
    } else {
      throw new BadRequestException('Wallet already exists.');
    }
  }

  /**
   *
   * @param companyId
   * @param employeeId
   * @param employeeData
   */
  public async editCompanyEmployeeWallet(companyId: number, id: number, employeeData: any) {
    const wallet = await this.walletRepository.findOne({
      id,
      companyId,
      type: WalletTypes.COMPANY,
      status: WalletStatuses.ACTIVE,
    }, { relations: ['user'] });
    if (wallet) {
      wallet.user.firstName = employeeData.firstName;
      wallet.user.lastName = employeeData.lastName;
      wallet.department = employeeData.department;
      wallet.title = employeeData.title;
      wallet.balancePerMonth = employeeData.balancePerMonth;
      wallet.balance = employeeData.balance;
      wallet.employeeNo = employeeData.employeeNO;
      wallet.user.image = employeeData.image;
      return await this.walletRepository.save(wallet);
    } else {
      throw new NotFoundException('Employee not found.');
    }
  }

  /**
   *
   * @param companyId
   * @param id
   */
  public async deleteCompanyEmployeeWallet(companyId: number, id: number) {
    const wallet = await this.walletRepository.findOne({
      id,
      companyId,
      type: WalletTypes.COMPANY,
      status: WalletStatuses.ACTIVE,
    }, { relations: ['user'] });
    if (wallet) {
      wallet.status = WalletStatuses.INACTIVE;
      return await this.walletRepository.save(wallet);
    } else {
      throw new NotFoundException('Employee not found.');
    }
  }

  /**
   *
   * @param companyId
   * @param id
   */
  public async deleteCompanyEmployeeWalletBulk(companyId: number, ids: any) {
    return await this.walletRepository.update({ companyId, id: In(ids) }, { status: WalletStatuses.INACTIVE });
  }

  /**
   * TODO: merchant has just one wallet, so i suggest that we can refactor this method to:
   * getMerchantWallet(merchantId: number): Promise<Wallet>
   *
   * @param merchantId
   */
  public async getMerchantWallets(merchantId: number): Promise<Wallet[]> {
    return await this.walletRepository.find({ merchantId, status: WalletStatuses.ACTIVE });
  }

  /**
   *
   * @param companyId
   * @param walletId
   * @param findOptions
   */
  public async getSingleCompanyWallet(companyId: number, walletId: number, findOptions: any): Promise<Wallet> {
    const { startDate, endDate, maxAmount, minAmount } = findOptions;
    let query = getRepository(Wallet)
      .createQueryBuilder('wallet')
      .leftJoinAndSelect('wallet.sourceTransactions', 'sourceTransactions')
      .leftJoinAndSelect('wallet.destinationTransactions', 'destinationTransactions')
      .leftJoinAndSelect('destinationTransactions.sourceFromDestinationWallet', 'sourceWallet')
      .leftJoinAndSelect('sourceTransactions.destinationFromSourceWallet', 'destinationWallet')
      .leftJoinAndSelect('sourceTransactions.qr', 'sourceQR')
      .leftJoinAndSelect('sourceWallet.merchant', 'sourceMerchant')
      .leftJoinAndSelect('destinationWallet.merchant', 'destinationMerchant')
      .select(
        [
          'wallet.id',
          'wallet.employeeNO',
          'wallet.balance',
          'wallet.department',
          'wallet.title',
          'wallet.createdAt',
          'wallet.balancePerMonth',
          'sourceTransactions.id',
          'sourceTransactions.description',
          'sourceTransactions.sourceBefore',
          'sourceTransactions.sourceAfter',
          'sourceTransactions.type',
          'sourceTransactions.createdAt',
          'sourceTransactions.amount',
          'destinationTransactions.id',
          'destinationTransactions.description',
          'destinationTransactions.destinationBefore',
          'destinationTransactions.destinationAfter',
          'destinationTransactions.type',
          'destinationTransactions.createdAt',
          'destinationTransactions.amount',
          'destinationWallet.status',
          'destinationWallet.id',
          'sourceWallet.status',
          'sourceWallet.id',
          'sourceMerchant.name',
          'sourceMerchant.logo',
          'sourceMerchant.phoneNumber',
          'sourceMerchant.id',
          'destinationMerchant.name',
          'destinationMerchant.logo',
          'destinationMerchant.phoneNumber',
          'destinationMerchant.id',
          'sourceQR.id',
          'sourceQR.code',
          'sourceQR.type',
          'sourceQR.amount',
        ])
      .andWhere('wallet.id = :walletId AND wallet.companyId = :companyId AND wallet.status = :status',
        { walletId, companyId, status: WalletStatuses.ACTIVE });

    if (startDate) {
      query.andWhere('( sourceTransactions.createdAt >= :startDate OR destinationTransactions.createdAt >= :startDate )', { startDate });
    }
    if (endDate) {
      query.andWhere('( sourceTransactions.createdAt <= :endDate OR destinationTransactions.createdAt <= :endDate )', { endDate });
    }
    if (maxAmount) {
      query.andWhere('( sourceTransactions.amount <= :maxAmount OR destinationTransactions.amount <= :maxAmount )', { maxAmount });
    }
    if (minAmount) {
      query.andWhere('( sourceTransactions.amount >= :minAmount OR destinationTransactions.amount >= :minAmount )', { minAmount });
    }

    const wallets = await query.getMany();

    if (wallets && wallets.length) {
      wallets.forEach((wallet) => {
        wallet.transactions = wallet.sourceTransactions.concat(wallet.destinationTransactions);
        delete wallet.sourceTransactions;
        delete wallet.destinationTransactions;
      });
      return wallets[0];
    }
    throw new NotFoundException('Wallet not found');
  }

  /**
   *
   * @param merchantId
   * @param walletId
   */
  public async getSingleMerchantWallet(merchantId: number, walletId: number): Promise<Wallet> {
    const wallet = await this.walletRepository.findOne({ id: walletId, merchantId, status: WalletStatuses.ACTIVE });
    if (wallet) {
      return wallet;
    }
    throw new NotFoundException('Wallet not found');
  }

  /**
   *
   * @param merchantId
   */
  public async findMerchantWallet(merchantId: number): Promise<Wallet> {
    const wallet = await this.walletRepository.findOne({ merchantId, status: WalletStatuses.ACTIVE });
    if (wallet) {
      return wallet;
    }
    throw new NotFoundException('Wallet not found');
  }

  /**
   *
   * @param walletId
   * @param amount
   */
  public async increaseBalance(walletId: number, amount: number): Promise<Wallet> {
    const currentWallet = await this.walletRepository.findOne({ id: walletId, status: WalletStatuses.ACTIVE });
    if (currentWallet) {
      currentWallet.balance += amount;
      return await this.walletRepository.save(currentWallet);
    }
    return null;
  }

  /**
   *
   * @param walletId
   * @param amount
   */
  public async decreaseBalance(walletId: number, amount: number): Promise<Wallet> {
    const currentWallet = await this.walletRepository.findOne({ id: walletId, status: WalletStatuses.ACTIVE });
    if (currentWallet) {
      currentWallet.balance -= amount;
      return await this.walletRepository.save(currentWallet);
    }
    return null;
  }

  /**
   *
   * @param merchantId
   */
  public async merchantIncomeFromLastSettlement(merchantId: number) {
    const merchantWallet = await this.findMerchantWallet(merchantId);
    if (merchantWallet) {
      const lastSettlement = await this.transactionsService.getLastMerchantSettlement(merchantWallet.id);
      const lastSettlementId = lastSettlement ? lastSettlement.id : 0;
      return await this.transactionsService.totalIncome(merchantWallet.id, lastSettlementId);
    } else {
      throw new NotFoundException('Wallet not found');
    }
  }

  /**
   *
   * @param merchantId
   */
  public async merchantTodayIncome(merchantId: number) {
    const merchantWallet = await this.findMerchantWallet(merchantId);
    if (merchantWallet) {
      return await this.transactionsService.todayIncome(merchantWallet.id);
    } else {
      throw new NotFoundException('Wallet not found');
    }
  }

  /**
   *
   * @param merchantId
   */
  public async merchantTodayTransactions(merchantId: number) {
    const merchantWallet = await this.findMerchantWallet(merchantId);
    if (merchantWallet) {
      const transactions = await this.transactionsService.todayTransactions(merchantWallet.id);
      const filteredTransactions = await this.filterTransactionRecordes(transactions, merchantWallet.id);
      return filteredTransactions;
    } else {
      throw new NotFoundException('Wallet not found');
    }
  }

  /**
   *
   * @param merchantId
   * @param options
   */
  public async merchantTransactions(merchantId: number, options: any) {
    const merchantWallet = await this.findMerchantWallet(merchantId);
    if (merchantWallet) {
      const transactions = await this.transactionsService.getAllWithDateFilter(merchantWallet.id, options.startDate, options.endDate);
      const filteredTransactions = await this.filterTransactionRecordes(transactions, merchantWallet.id);
      return filteredTransactions;
    } else {
      throw new NotFoundException('Wallet not found');
    }
  }

  /**
   *
   * @param merchantId
   * @param transactionId
   */
  public async getSingleMerchantTransaction(merchantId: number, transactionId: number) {
    const merchantWallet = await this.findMerchantWallet(merchantId);
    if (merchantWallet) {
      const transaction = await this.transactionsService.getByIdAndWallet(merchantWallet.id, transactionId);
      const filteredTransactions = await this.filterTransactionRecordes([transaction], merchantWallet.id);
      return filteredTransactions[0];
    } else {
      throw new NotFoundException('Wallet not found');
    }
  }

  /**
   *
   * @param merchantId
   */
  public async merchantSettlements(merchantId: number) {
    const merchantWallet = await this.findMerchantWallet(merchantId);
    if (merchantWallet) {
      const settlements = await this.invoiceService.getAllSettlementsByWallet(merchantWallet.id);
      const filteredSettlements = await this.filterSettlementRecordes(settlements, merchantWallet.id);
      return filteredSettlements;
    } else {
      throw new NotFoundException('Wallet not found');
    }
  }

  /**
   *
   * @param merchantId
   * @param settlementId
   */
  public async merchantSingleSettlement(merchantId: number, settlementId: number) {
    const merchantWallet = await this.findMerchantWallet(merchantId);
    if (merchantWallet) {
      const settlement = await this.invoiceService.getSingleSettlementsByIdAndWallet(merchantWallet.id, settlementId);
      const filteredSettlement = await this.filterSettlementRecordes([settlement], merchantWallet.id);
      return filteredSettlement[0];
    } else {
      throw new NotFoundException('Wallet not found');
    }
  }

  /**
   * Private Scope
   *
   */
  /**
   *
   * @param records
   * @param merchantWalletId
   */
  private async filterTransactionRecordes(records: Transaction[], merchantWalletId: number) {
    if (records && records.length) {
      records.forEach((record) => {
        if (record.source === merchantWalletId) {
          delete record.destination;
          delete record.destinationWallet;
          delete record.destinationBefore;
          delete record.destinationAfter;
        } else {
          delete record.source;
          delete record.sourceWallet;
          delete record.sourceBefore;
          delete record.sourceAfter;
        }
      });
    }
    return records;
  }

  /**
   *
   * @param records
   * @param merchantWalletId
   */
  private async filterSettlementRecordes(records: Invoice[], merchantWalletId: number) {
    if (records && records.length) {
      records.forEach((record) => {
        if (record.transactions && record.transactions.length) {
          this.filterTransactionRecordes(record.transactions, merchantWalletId);
        }
      });
    }
    return records;
  }
}

