import { Module, forwardRef } from '@nestjs/common';
import { WalletsController }  from './wallets.controller';
import { WalletsService }     from './wallets.service';
import { AuthModule }         from '../auth/auth.module';
import { AclModule }          from '../acl/acl.module';
import { TypeOrmModule }      from '@nestjs/typeorm';
import { WalletsRepository }  from './wallets.repository';
import { TransactionsModule } from 'src/transactions/transactions.module';
import { InvoicesModule }     from '../invoices/invoices.module';
import { UsersModule }        from '../users/users.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([WalletsRepository]),
    forwardRef(() => TransactionsModule),
    forwardRef(() => UsersModule),
    forwardRef(() => AuthModule),
    AclModule,
    InvoicesModule,
  ],
  controllers: [WalletsController],
  providers: [WalletsService],
  exports: [WalletsService],
})
export class WalletsModule {
}
