enum WalletStatuses {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE'
}

export default WalletStatuses;
