enum WalletTypes {
  PERSONAL = 'PERSONAL',
  COMPANY = 'COMPANY',
  MERCHANT = 'MERCHANT'
}

export default WalletTypes;
