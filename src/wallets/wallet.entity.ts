import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne, OneToMany, JoinColumn,
}                      from 'typeorm';
import { User }        from '../users/user.entity';
import { Merchant }    from '../merchants/merchant.entity';
import { Company }     from '../companies/company.entity';
import { Invoice }     from '../invoices/invoice.entity';
import { Transaction } from '../transactions/transaction.entity';
import WalletTypes     from './constants/walletTypes.enum';
import WalletStatuses  from './constants/walletStatuses.enum';
import { Exclude }     from 'class-transformer';

@Entity({
  name: 'wallets',
})

export class Wallet {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'user_id',
    nullable: true,
  })
  userId: number;

  @Column({
    name: 'company_id',
    nullable: true,
  })
  companyId: number;

  @Column({
    name: 'merchant_id',
    nullable: true,
  })
  merchantId: number;

  @Column({
    name: 'employee_no',
    nullable: true,
  })
  employeeNo: number;

  @Column({
    name: 'balance',
    nullable: true,
    default: 0,
  })
  balance: number;

  @Column({
    name: 'status',
    nullable: true,
  })
  status: WalletStatuses;

  @Column({
    name: 'type',
  })
  type: WalletTypes;

  @Column({
    name: 'department',
    nullable: true,
  })
  department: string;

  @Column({
    name: 'title',
    nullable: true,
  })
  title: string;

  @Column({
    name: 'balance_per_month',
    nullable: true,
  })
  @Exclude()
  balancePerMonth: number;

  @CreateDateColumn({
    name: 'created_at',
  })
  createdAt: Date;

  @UpdateDateColumn({
    name: 'updated_at',
  })
  updatedAt: Date;

  @ManyToOne(type => User, user => user.wallets, {
    eager: true,
  })
  @JoinColumn({ name: 'user_id' })
  user: User;

  @ManyToOne(type => Merchant, merchant => merchant.wallets, {
    eager: true,
  })
  @JoinColumn({ name: 'merchant_id' })
  merchant: Merchant;

  @ManyToOne(type => Company, company => company.wallets, {
    eager: true,
  })
  @JoinColumn({ name: 'company_id' })
  company: Company;

  // @ManyToOne(type => User, employee => employee.employeeWallets)
  // @JoinColumn({ name: 'employee_id' })
  // employee: User;

  @OneToMany(type => Invoice, invoice => invoice.wallet)
  invoices: Invoice[];

  @OneToMany(type => Transaction, transaction => transaction.sourceWallet)
  sourceTransactions: Transaction[];

  @OneToMany(type => Transaction, transaction => transaction.destinationWallet)
  destinationTransactions: Transaction[];
  transactions: any;
}
