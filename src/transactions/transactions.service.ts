import { BadRequestException, ForbiddenException, Injectable, NotFoundException, OnModuleInit } from '@nestjs/common';
import { InjectRepository }                                                                     from '@nestjs/typeorm';
import { TransactionsRepository }                                                               from './transactions.repository';
import { Transaction }                                                                          from './transaction.entity';
import { Wallet }                                                                               from '../wallets/wallet.entity';
import { QR }                                                                                   from '../qrs/qr.entity';
import TransactionType
                                                                                                from './constants/transactionTypes.enum';
import { WalletsService }                                                                       from '../wallets/wallets.service';
import { getRepository }                                                                        from 'typeorm';
import RefundTransactionDto
                                                                                                from './dto/refund-transaction.dto';
import { AclService }                                                                           from '../acl/acl.service';
import MerchantRole
                                                                                                from '../acl/constants/merchant-roles.enum';
import { ModuleRef }                                                                            from '@nestjs/core';

const Hashids = require('hashids/cjs');


@Injectable()
export class TransactionsService implements OnModuleInit {

  private walletsService: WalletsService;
  private hashids = null;

  constructor(
    @InjectRepository(TransactionsRepository)
    private readonly transactionsRepository: TransactionsRepository,
    private readonly aclService: AclService,
    private readonly moduleRef: ModuleRef,
  ) {
    this.hashids = new Hashids();
  }

  /**
   * Load circular dependency modules
   *
   */
  onModuleInit(): any {
    this.walletsService = this.moduleRef.get(WalletsService, { strict: false });
  }

  /**
   * Find a transaction by id
   *
   * @param userId
   * @param transactionId
   */
  public async getUserTransaction(userId: number, transactionId: number): Promise<any> {
    const transaction = await this.transactionsRepository.findById(transactionId);

    // Check ownership;
    const sourceOwner = (transaction.sourceWallet || {}).userId;
    const destinationOwner = (transaction.destinationWallet || {}).userId;

    if (sourceOwner === userId || destinationOwner === userId) {
      return {
        id: transaction.id,
        amount: transaction.amount,
        type: transaction.type,
        createdAt: transaction.createdAt,
        merchant: {
          name: transaction.qr.merchant.name,
          logo: transaction.qr.merchant.logo,
        },
      };
    }

    throw new ForbiddenException('You have not sufficient permission');
  }

  /**
   * Find all transactions which is related to this user
   *
   * @param userId
   * @param filter
   * @param page
   * @param limit
   */
  public async getPaginatedUserTransaction(userId: number, filter: any, page: number, limit: number)
    : Promise<{
    transactions: any[],
    total: number
  }> {
    // Find user's wallets
    const wallets = await this.walletsService.getAllUserWallets(userId);

    // Extract wallets' id's
    const walletIds = wallets.map(wallet => +wallet.id);

    // Find transactions for those wallets
    const [rawTransactions, total] = await this.transactionsRepository.findAndCountByWalletIds(walletIds, page, limit);

    // Decorate transactions
    const transactions = rawTransactions.map(transaction => {
      return {
        id: transaction.id,
        amount: transaction.amount,
        type: transaction.type,
        createdAt: transaction.createdAt,
        merchant: {
          name: transaction.qr.merchant.name,
          logo: transaction.qr.merchant.logo,
        },
      };
    });

    return {
      transactions,
      total,
    };
  }

  /**
   *
   * @param userId: number
   * @param transactionId: number
   * @param refundTransactionDto: RefundTransactionDto
   */
  public async refund(userId: number, transactionId: number, refundTransactionDto: RefundTransactionDto): Promise<Transaction> {

    const mainTransaction = await this.transactionsRepository.findOne({
      id: transactionId,
    }, {
      relations: ['sourceWallet', 'destinationWallet', 'qr'],
    });

    if (!mainTransaction) {
      throw new NotFoundException('Transaction not found');
    }

    // Check ownership
    await this.aclService.checkMerchantPermission(
      userId,
      mainTransaction.destinationWallet.merchantId,
      [
        MerchantRole.CASHIER,
        MerchantRole.OWNER,
      ],
    );

    const previousRefunds = await getRepository(Transaction).createQueryBuilder()
      .where('description @> :description', {
        description: {
          mainTransactionId: transactionId,
        },
      }).getMany();

    if (previousRefunds && previousRefunds.length) {
      const totalRefunds = previousRefunds
        .map(transaction => transaction.amount)
        .reduce((total, amount) => {
          return total + amount;
        });

      if (totalRefunds >= mainTransaction.amount) {
        throw new BadRequestException('This transaction has been refund totally! You can not refund it again.');
      }
    }

    let amount = mainTransaction.amount;
    if (refundTransactionDto.amount) {
      amount = refundTransactionDto.amount;
    }

    return await this.transfer(
      mainTransaction.destinationWallet,
      mainTransaction.sourceWallet,
      amount,
      TransactionType.REFUND,
      mainTransaction.qr,
      {
        reason: refundTransactionDto.reason,
        mainTransactionId: transactionId,
      });
  }

  /**
   * Register a new transaction and update wallets
   *
   * @param sourceWallet
   * @param destinationWallet
   * @param amount
   * @param type
   * @param qr
   * @param description
   */
  public async transfer(
    sourceWallet: Wallet,
    destinationWallet: Wallet,
    amount: number,
    type: TransactionType,
    qr?: QR,
    description?: {
      reason: string,
      mainTransactionId: number,
    },
  ): Promise<Transaction> {
    const transaction = new Transaction();

    if (sourceWallet.balance < amount) {
      throw new BadRequestException('Balance is not enough!');
    }

    transaction.amount = amount;
    transaction.type = type;
    transaction.qr = qr;
    transaction.description = description;

    transaction.source = sourceWallet.id;
    transaction.sourceWallet = sourceWallet;
    transaction.sourceBefore = sourceWallet.balance;
    transaction.sourceAfter = sourceWallet.balance - amount;

    transaction.destination = destinationWallet.id;
    transaction.destinationWallet = destinationWallet;
    transaction.destinationBefore = destinationWallet.balance;
    transaction.destinationAfter = destinationWallet.balance + amount;

    const storedTransaction = await this.transactionsRepository.save(transaction);

    if (storedTransaction) {
      transaction.refNumber = this.hashids.encode(transaction.id);
      transaction.isAddedToSource = transaction.sourceAfter > transaction.sourceBefore;
      await this.transactionsRepository.save(transaction);
    }

    return storedTransaction;
  }

  /**
   *
   * @param walletId
   */
  public async getLastMerchantSettlement(walletId) {
    const lastTransaction = await this.transactionsRepository.findOne({
      destination: walletId,
      type: TransactionType.SETTLEMENT,
    }, { order: { id: 'DESC' } });

    return lastTransaction;
  }

  /**
   *
   * @param walletId
   * @param lastSettlementId
   */
  public async totalIncome(walletId: number, lastSettlementId: number) {
    const totalIncome = await getRepository(Transaction).createQueryBuilder()
      .andWhere('destination_id = :walletId', { walletId })
      .andWhere('id > :lastSettlementId', { lastSettlementId })
      .andWhere('type = :income', { income: TransactionType.QR })
      .select('SUM(amount)', 'sum')
      .getRawOne();

    return totalIncome ? +totalIncome.sum : 0;
  }

  /**
   *
   * @param walletId
   */
  public async todayIncome(walletId: number) {
    const dayBoundries = this.getTodayBoundries();
    const totalIncome = await getRepository(Transaction).createQueryBuilder()
      .andWhere('destination_id = :walletId', { walletId })
      .andWhere('type = :income', { income: TransactionType.QR })
      .andWhere('Transaction.createdAt >= :startOfTheDay', { startOfTheDay: dayBoundries.start })
      .andWhere('Transaction.createdAt <= :endOfTheDay', { endOfTheDay: dayBoundries.end })
      .select('SUM(amount)', 'sum')
      .addSelect('count(*)', 'total')
      .getRawOne();

    if (totalIncome) {
      return { sum: +totalIncome.sum, total: +totalIncome.total };
    }
    return { sum: 0, total: 0 };
  }

  /**
   *
   * @param walletId
   */
  public async todayTransactions(walletId: number) {
    const dayBoundries = this.getTodayBoundries();
    const todayTransactions = await getRepository(Transaction).createQueryBuilder()
      .andWhere('( destination_id = :destinationWalletId OR source_id = :sourceWalletId )', {
        destinationWalletId: walletId,
        sourceWalletId: walletId,
      })
      .andWhere('Transaction.createdAt >= :startOfTheDay', { startOfTheDay: dayBoundries.start })
      .andWhere('Transaction.createdAt <= :endOfTheDay', { endOfTheDay: dayBoundries.end })
      .getMany();

    return todayTransactions;
  }

  /**
   *
   * @param walletId
   * @param startDate
   * @param endDate
   */
  public async getAllWithDateFilter(walletId: number, startDate, endDate) {
    const transactions = getRepository(Transaction).createQueryBuilder()
      .andWhere('( destination_id = :destinationWalletId OR source_id = :sourceWalletId )', {
        destinationWalletId: walletId,
        sourceWalletId: walletId,
      });

    if (startDate) {
      transactions.andWhere('Transaction.createdAt >= :startDate', { startDate });
    }
    if (endDate) {
      transactions.andWhere('Transaction.createdAt <= :endDate', { endDate });
    }

    return await transactions.getMany();
  }

  /**
   *
   * @param walletId
   * @param id
   */
  public async getByIdAndWallet(walletId, id) {
    const transaction = await this.transactionsRepository.findOne({ id },
      { relations: ['sourceWallet', 'destinationWallet'] });
    if (transaction) {
      if (transaction.source === walletId || transaction.destination === walletId) {
        return transaction;
      } else {
        throw new ForbiddenException('The transaction does not belong to this Merchant');
      }
    } else {
      throw new NotFoundException('Transaction not found');
    }
  }

  /**
   * Private Scope
   *
   */

  /**
   *
   */
  private getTodayBoundries() {
    const date = new Date();
    let month = +date.getMonth();
    const year = date.getFullYear();
    const day = date.getDate();
    month++;
    const fullDate = year + '-' + month + '-' + day + ' ';
    return { start: fullDate + '00:00:00', end: fullDate + '23:59:59' };
  }
}
