import { forwardRef, Module }     from '@nestjs/common';
import { TypeOrmModule }          from '@nestjs/typeorm';
import { TransactionsRepository } from './transactions.repository';
import { TransactionsController } from './transactions.controller';
import { TransactionsService }    from './transactions.service';
import { AuthModule }             from '../auth/auth.module';
import { AclModule }              from '../acl/acl.module';
import { WalletsModule }          from 'src/wallets/wallets.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([TransactionsRepository]),
    forwardRef(() => WalletsModule),
    forwardRef(() => AuthModule),
    AclModule,
  ],
  controllers: [TransactionsController],
  providers: [TransactionsService],
  exports: [TransactionsService],
})
export class TransactionsModule {
}
