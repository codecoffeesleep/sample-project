enum TransactionType {
  QR = 'QR',
  REFUND = 'REFUND',
  SETTLEMENT = 'SETTLEMENT',
  WALLET_RECHARGE = 'PURCHASE',
}

export default TransactionType;
