import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
  BaseEntity,
} from 'typeorm';
import { Invoice }      from '../invoices/invoice.entity';
import { QR }           from '../qrs/qr.entity';
import { Wallet }       from '../wallets/wallet.entity';
import TransactionType  from './constants/transactionTypes.enum';

@Entity({
  name: 'transactions',
})
export class Transaction extends BaseEntity{

  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'amount',
  })
  amount: number;

  @Column({
    name: 'source_id',
  })
  source: number;

  @Column({
    name: 'destination_id',
  })
  destination: number;

  @Column({
    name: 'source_before',
  })
  sourceBefore: number;

  @Column({
    name: 'source_after',
  })
  sourceAfter: number;

  @Column({
    name: 'destination_before',
  })
  destinationBefore: number;

  @Column({
    name: 'destination_after',
  })
  destinationAfter: number;

  @Column({
    name: 'ref_number',
  })
  refNumber: string;

  @Column({
    name: 'is_added_to_source',
  })
  isAddedToSource: boolean;

  @Column({
    name: 'type',
  })
  type: TransactionType;

  @Column({
    type: 'jsonb',
    array: false,
    default: () => "{}",
    nullable: true,
    name: 'description'
  })
  description: object

  @CreateDateColumn({
    name: 'created_at',
    nullable: true,
  })
  createdAt: Date;

  @UpdateDateColumn({
    name: 'updated_at',
    nullable: true,
  })
  updatedAt: Date;

  @ManyToOne(type => Invoice, invoice => invoice.transactions, { nullable: true })
  @JoinColumn({ name: 'invoice_id' })
  invoice?: Invoice;

  @ManyToOne(type => QR, qr => qr.transactions, { nullable: true })
  @JoinColumn({ name: 'qr_id' })
  qr: QR;

  @ManyToOne(type => Wallet, source => source.sourceTransactions)
  @JoinColumn({ name: 'source_id' })
  sourceWallet: Wallet;

  @ManyToOne(type => Wallet, source => source.destinationTransactions)
  @JoinColumn({ name: 'source_id' })
  sourceFromDestinationWallet: Wallet;

  @ManyToOne(type => Wallet, destination => destination.destinationTransactions)
  @JoinColumn({ name: 'destination_id' })
  destinationWallet: Wallet;

  @ManyToOne(type => Wallet, destination => destination.sourceTransactions)
  @JoinColumn({ name: 'destination_id' })
  destinationFromSourceWallet: Wallet;

}
