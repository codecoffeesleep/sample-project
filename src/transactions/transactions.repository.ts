import { EntityRepository, Repository } from 'typeorm';
import { Transaction }                  from './transaction.entity';
import { NotFoundException }            from '@nestjs/common';


@EntityRepository(Transaction)
export class TransactionsRepository extends Repository<Transaction> {

  /**
   * Find all transactions which are related to these wallets
   *
   * @param walletIds
   * @param page
   * @param limit
   */
  public async findAndCountByWalletIds(walletIds: number[], page: number, limit: number): Promise<[Transaction[], number]> {
    return this.createQueryBuilder('transaction')
      .leftJoin('transaction.qr', 'qr')
      .leftJoin('qr.merchant', 'merchant')
      .where('transaction.source IN (:...walletIds)', { walletIds })
      .orWhere('transaction.destination IN (:...walletIds)', { walletIds })
      .orderBy('transaction.createdAt', 'DESC')
      .select([
        'transaction.id',
        'transaction.amount',
        'transaction.type',
        'transaction.createdAt',
        'qr.code',
        'merchant.name',
        'merchant.logo',
      ])
      .take(limit)
      .skip((page - 1) * limit)
      .getManyAndCount();
  }

  /**
   * Find single transactions
   *
   * @param transactionId
   */
  public async findById(transactionId: number): Promise<Transaction> {
    const transaction = this.createQueryBuilder('transaction')
      .leftJoin('transaction.qr', 'qr')
      .leftJoin('qr.merchant', 'merchant')
      .innerJoin('transaction.sourceWallet', 'sourceWallet')
      .leftJoin('transaction.destinationWallet', 'destinationWallet')
      .where('transaction.id = :transactionId', { transactionId })
      .select([
        'transaction.id',
        'transaction.amount',
        'transaction.type',
        'transaction.createdAt',
        'qr.code',
        'merchant.name',
        'merchant.logo',
        'sourceWallet.userId',
        'destinationWallet.userId',
      ])
      .getOne();

    if (!transaction) {
      throw new NotFoundException('Transaction not found');
    }

    return transaction;
  }

}
