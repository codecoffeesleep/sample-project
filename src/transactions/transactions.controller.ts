import { Body, Controller, Get, Param, ParseIntPipe, Post, Query, Req, UseGuards } from '@nestjs/common';
import { AuthGuard }                                                               from '@nestjs/passport';
import { Request }                                                                 from 'express';
import { TransactionsService }                                                     from './transactions.service';
import RefundTransactionDto
                                                                                   from './dto/refund-transaction.dto';
import { ControllerResponse }                                                      from '../common/response-decorator/responses.interface';
import PaginationDto                                                               from '../common/dto/pagination.dto';

@Controller('transactions')
@UseGuards(AuthGuard())
export class TransactionsController {

  /**
   *
   * @param transactionsService
   */
  constructor(private readonly transactionsService: TransactionsService) {
  }

  @Get('/')
  public async getAllTransactions(@Req() request: Request, @Query() query: PaginationDto): Promise<ControllerResponse> {
    const user = request['user'];
    const { page, limit, filter } = query;
    const result = await this.transactionsService.getPaginatedUserTransaction(user.id, filter, +page, +limit);
    return {
      data: result.transactions,
      total: result.total,
    };
  }

  @Get('/:id')
  public async getSingleTransaction(@Req() request: Request, @Param('id') transactionId: string): Promise<ControllerResponse> {
    const user = request['user'];
    const transaction = await this.transactionsService.getUserTransaction(user.id, +transactionId);
    return {
      data: transaction,
    };
  }

  @Post(':transactionId/refund')
  public async refund(
    @Req() request: Request,
    @Body() refundTransactionDto: RefundTransactionDto,
    @Param('transactionId', ParseIntPipe) transactionId: number,
  ) {
    const user = request['user'];
    return await this.transactionsService.refund(user.id, transactionId, refundTransactionDto);
  }
}
