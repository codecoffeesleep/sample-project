import { IsNumber, IsOptional, IsString } from 'class-validator';

export default class RefundTransactionDto {
  @IsOptional()
  @IsString()
  reason: string;

  @IsOptional()
  @IsNumber()
  amount: number;
}
