import { forwardRef, Module }  from '@nestjs/common';
import { TypeOrmModule }       from '@nestjs/typeorm';
import { CompaniesService }    from './companies.service';
import { CompaniesController } from './companies.controller';
import { Company }             from './company.entity';
import { WalletsService }      from '../wallets/wallets.service';
import { WalletsRepository }   from '../wallets/wallets.repository';
import { AclModule }           from '../acl/acl.module';
import { AuthModule }          from '../auth/auth.module';
import { TransactionsModule }  from '../transactions/transactions.module';
import { WalletsModule }       from '../wallets/wallets.module';
import { CompaniesRepository } from './companies.repository';

@Module({
  imports: [
    AclModule,
    AuthModule,
    TypeOrmModule.forFeature([Company, CompaniesRepository]),
    WalletsModule
  ],
  providers: [CompaniesService],
  controllers: [CompaniesController],
  exports: [CompaniesService]
})
export class CompaniesModule {

}
