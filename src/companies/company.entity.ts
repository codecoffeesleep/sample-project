import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, OneToMany } from 'typeorm';
import { Wallet }                                                                                from '../wallets/wallet.entity';
import { CompanyToUser } from '../acl/entites/companyToUser.entity';

@Entity({
  name: 'companies',
})
export class Company {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'name',
  })
  name: string;

  @Column({
    name: 'logo',
  })
  logo: string;

  @Column({
    name: 'legal_name',
  })
  legalName: string;

  @Column({
    name: 'address',
  })
  address: string;

  @Column({
    type: 'point',
    nullable: true,
  })
  location: any;

  @Column({
    name: 'phone_number',
    nullable: true,
  })
  phoneNumber: string;

  @CreateDateColumn({
    name: 'created_at'
  })
  createdAt: Date;

  @UpdateDateColumn({
    name: 'updated_at'
  })
  updatedAt: Date;

  @OneToMany(type => CompanyToUser, userCompany => userCompany.company)
  userCompanies: CompanyToUser[];

  @OneToMany(type => Wallet, wallet => wallet.company)
  wallets: Wallet[];

}
