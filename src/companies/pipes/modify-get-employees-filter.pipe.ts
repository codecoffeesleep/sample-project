import { PipeTransform, Injectable, ArgumentMetadata } from '@nestjs/common';
import GetEmployeesDTO                                 from '../dto/getEmployees.dto';

@Injectable()
export class ModifyGetEmployeesFilterPipe implements PipeTransform<GetEmployeesDTO, any> {
  transform(value: GetEmployeesDTO, metadata: ArgumentMetadata): any {
    const minBalance = value['min_balance'] ? parseInt(value['min_balance']) : undefined;
    const maxBalance = value['max_balance'] ? parseInt(value['max_balance']) : undefined;
    const minAmount = value['min_amount'] ? parseInt(value['min_amount']) : undefined;
    const maxAmount = value['max_amount'] ? parseInt(value['max_amount']) : undefined;
    const startDate = value['start_date'] || undefined
    const endDate = value['end_date'] || undefined

    return { minBalance, maxBalance, startDate, endDate, minAmount, maxAmount };
  }
}
