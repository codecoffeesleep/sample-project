import {
  Get,
  Controller,
  UseGuards,
  SetMetadata,
  Req,
  Res,
  Param,
  Post,
  Body, Patch, Delete, Query, HttpStatus,
} from '@nestjs/common';
import { CheckPermissionGuard } from '../acl/guards/checkPermission.guard';
import { Company }              from './company.entity';
import CompanyRole                 from '../acl/constants/company-roles.enum';
import { Request, Response }            from 'express';
import { WalletsService }               from '../wallets/wallets.service';
import { AuthGuard }                    from '@nestjs/passport';
import AddEmployeeDTO                   from './dto/addEmployee.dto';
import EditEmployeeDTO                  from './dto/editEmployee.dto';
import EmployeeIdsDto                   from './dto/employeeIds.dto';
import GetEmployeesDTO                  from './dto/getEmployees.dto';
import { ModifyGetEmployeesFilterPipe } from './pipes/modify-get-employees-filter.pipe';
import GetSingleEmployeeDTO             from './dto/getSingleEmployee.dto';
import { I18nService } from 'nestjs-i18n';
import { CompaniesService } from './companies.service';

@Controller('companies')
@UseGuards(AuthGuard())
export class CompaniesController {

  /**
   *
   * @param walletService
   * @param companiesService
   * @param i18n
   */
  constructor(
    private readonly walletService: WalletsService,
    private readonly companiesService: CompaniesService,
    private readonly i18n: I18nService
  ) {
  }


  /**
   * Represent the list of employees of a certain company
   *
   * @param request
   * @param response
   * @param companyId
   */
  @Get('/:resource_id/employees')
  @UseGuards(CheckPermissionGuard)
  @SetMetadata('acl', {
      resource: Company,
      roles: [
        CompanyRole.OWNER,
        CompanyRole.CASHIER,
      ],
    },
  )
  public async allCompanyEmployeeWallets(
    @Req() request: Request,
    @Res() response: Response,
    @Param('resource_id') companyId,
    @Query(new ModifyGetEmployeesFilterPipe()) getEmployeesDTO: GetEmployeesDTO
  ) {
    const wallets = await this.walletService.getCompanyEmployeeWallets(companyId, getEmployeesDTO);
    return {
      httpStatus: HttpStatus.OK,
      data: wallets,
      total: wallets.length
    };
  }

  /**
   *
   * @param request
   * @param response
   * @param companyId
   * @param addEmployeeDto
   */
  @Post('/:resource_id/employees')
  @UseGuards(CheckPermissionGuard)
  @SetMetadata('acl', {
      resource: Company,
      roles: [
        CompanyRole.OWNER,
        CompanyRole.CASHIER,
      ],
    },
  )
  public async addEmployee(
    @Req() request: Request,
    @Res() response: Response,
    @Param('resource_id') companyId,
    @Body() addEmployeeDto: AddEmployeeDTO
  ) {
    const newCompany = await this.walletService.addCompanyEmployeeWallet(companyId, addEmployeeDto);
    return {
      httpStatus: HttpStatus.CREATED,
      data: newCompany
    };
  }

  /**
   *
   * @param request
   * @param response
   * @param companyId
   * @param editEmployeeDto
   */
  @Patch('/:resource_id/employees/:id')
  @UseGuards(CheckPermissionGuard)
  @SetMetadata('acl', {
      resource: Company,
      roles: [
        CompanyRole.OWNER,
        CompanyRole.CASHIER,
      ],
    },
  )
  public async editEmployee(@Req() request: Request, @Res() response: Response, @Param('resource_id') companyId, @Body() editEmployeeDto: EditEmployeeDTO, @Param('id') employeeId) {
    const newCompany = await this.walletService.editCompanyEmployeeWallet(companyId, employeeId, editEmployeeDto);
    return {
      httpStatus: HttpStatus.ACCEPTED,
      data: newCompany
    };
  }

  /**
   *
   * @param request
   * @param response
   * @param companyId
   * @param employeeId
   */
  @Delete('/:resource_id/employees/:id')
  @UseGuards(CheckPermissionGuard)
  @SetMetadata('acl', {
      resource: Company,
      roles: [
        CompanyRole.OWNER,
        CompanyRole.CASHIER,
      ],
    },
  )
  public async deleteEmployee(@Req() request: Request, @Res() response: Response, @Param('resource_id') companyId, @Param('id') employeeId) {
    await this.walletService.deleteCompanyEmployeeWallet(companyId, employeeId);
    return {
      httpStatus: HttpStatus.ACCEPTED
    };
  }

  /**
   *
   * @param request
   * @param response
   * @param companyId
   * @param employeeIdsDto
   */
  @Delete('/:resource_id/employees')
  @UseGuards(CheckPermissionGuard)
  @SetMetadata('acl', {
      resource: Company,
      roles: [
        CompanyRole.OWNER,
        CompanyRole.CASHIER,
      ],
    },
  )
  public async deleteEmployeeBulk(@Req() request: Request, @Res() response: Response, @Param('resource_id') companyId, @Body() employeeIdsDto: EmployeeIdsDto) {
    await this.walletService.deleteCompanyEmployeeWalletBulk(companyId, employeeIdsDto.ids);
    return {
      httpStatus: HttpStatus.ACCEPTED
    };
  }

  /**
   * Get the list of wallets of a certain company
   *
   * @param request
   * @param response
   * @param companyId
   */
  @Get('/:resource_id/wallets')
  @UseGuards(CheckPermissionGuard)
  @SetMetadata('acl', {
      resource: Company,
      roles: [
        CompanyRole.OWNER,
        CompanyRole.CASHIER,
      ],
    },
  )
  public async allCompanyWallets(
    @Req() request: Request,
    @Res() response: Response,
    @Param('resource_id') companyId
  ) {
    const wallets = await this.walletService.getCompanyWallets(companyId);
    return {
      httpStatus: HttpStatus.OK,
      data: wallets,
      total: wallets.length
    };
  }

  /**
   *
   * @param request
   * @param response
   * @param companyId
   * @param id
   */
  @Get('/:resource_id/wallets/:id')
  @UseGuards(CheckPermissionGuard)
  @SetMetadata('acl', {
      resource: Company,
      roles: [
        CompanyRole.OWNER,
        CompanyRole.CASHIER,
      ],
    },
  )
  public async singleCompanyWallet(
    @Req() request: Request,
    @Res() response: Response,
    @Param('resource_id') companyId,
    @Param('id') id,
    @Query(new ModifyGetEmployeesFilterPipe()) getSingleEmployeeDTO: GetSingleEmployeeDTO
  ) {
    const wallet = await this.walletService.getSingleCompanyWallet(companyId, id, getSingleEmployeeDTO);
    return {
      httpStatus: HttpStatus.OK,
      data: wallet
    };
  }

  /**
   *
   * @param request
   * @param response
   * @param companyId
   */
  @Get('/:resource_id')
  @UseGuards(CheckPermissionGuard)
  @SetMetadata('acl', {
      resource: Company,
      roles: [
        CompanyRole.OWNER,
        CompanyRole.CASHIER
      ],
    },
  )
  public async singleCompany(
    @Req() request: Request,
    @Res() response: Response,
    @Param('resource_id') companyId
  ) {
    //TODO prepare the logo of the company
    const company = await this.companiesService.findOne(companyId);
    return {
      httpStatus: HttpStatus.OK,
      data: company
    };
  }
}
