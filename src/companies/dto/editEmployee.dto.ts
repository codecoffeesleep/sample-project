import { IsString, IsOptional, IsNumber } from 'class-validator';

export default class EditEmployeeDTO {

  @IsString()
  @IsOptional()
  firstName: string;

  @IsString()
  @IsOptional()
  lastName: string;

  @IsString()
  @IsOptional()
  department: string;

  @IsString()
  @IsOptional()
  title: string;

  @IsNumber()
  @IsOptional()
  balancePerMonth: number;

  @IsNumber()
  @IsOptional()
  balance: number;

  @IsString()
  @IsOptional()
  employeeNO: string;

  @IsString()
  @IsOptional()
  image: string;
}
