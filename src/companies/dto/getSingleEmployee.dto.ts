import { IsOptional, IsString, Matches } from 'class-validator';

export default class GetSingleEmployeeDTO {

  @IsOptional()
  min_amount: string;

  @IsOptional()
  max_amount: string;

  @IsString()
  @IsOptional()
  @Matches(/^\d{4}-([0-9]|0[1-9]|1[0-2])-([0-9]|0[1-9]|[12][0-9]|3[01])$/)
  start_date: string;

  @IsString()
  @IsOptional()
  @Matches(/^\d{4}-([0-9]|0[1-9]|1[0-2])-([0-9]|0[1-9]|[12][0-9]|3[01])$/)
  end_date: string;
}
