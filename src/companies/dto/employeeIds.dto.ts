import { IsArray } from 'class-validator';

export default class EmployeeIdsDto {

  @IsArray()
  ids: object;

}
