import { EntityRepository, Repository } from 'typeorm';
import { Notification }                     from './notification.entity';

@EntityRepository(Notification)
export class NotificationsRepository extends Repository<Notification> {

}
