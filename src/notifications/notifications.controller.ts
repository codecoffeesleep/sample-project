import { Controller, Get, Param, Req, Res, UseGuards, Patch, HttpStatus } from '@nestjs/common';
import { AuthGuard }                                                      from '@nestjs/passport';
import { Request, Response }                                              from 'express';
import { NotificationsService }                                           from './notifications.service';

@Controller('notifications')
@UseGuards(AuthGuard())
export class NotificationsController {

  constructor(
    private readonly notificationsService: NotificationsService,
  ) {
  }

  /**
   *
   * @param request
   * @param response
   */
  @Get()
  public async getAll(@Req() request: Request, @Res() response: Response) {
    const userId = request['user'].id;
    const notifications = await this.notificationsService.getAllByUserId(userId);
    return {
      data: notifications,
      total: notifications.length,
    };
  }

  /**
   *
   * @param request
   * @param response
   */
  @Get('unread-count')
  public async getUnreadCount(@Req() request: Request, @Res() response: Response) {
    const userId = request['user'].id;
    const count = await this.notificationsService.getUnreadCountByUserId(userId);
    return {
      data: { unreadCount: count },
    };
  }

  /**
   *
   * @param request
   * @param response
   * @param notificationId
   */
  @Patch(':id')
  public async markAsRead(@Req() request: Request, @Res() response: Response, @Param('id') notificationId) {
    const userId = request['user'].id;
    await this.notificationsService.markAsRead(userId, notificationId);
    return {
      httpStatus: HttpStatus.ACCEPTED,
    };
  }
}
