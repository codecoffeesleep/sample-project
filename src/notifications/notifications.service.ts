import { BadRequestException, ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';
import { NotificationsRepository }                                                from './notifications.repository';

@Injectable()
export class NotificationsService {

  constructor(
    private readonly notificationsRepository: NotificationsRepository
  ) {
  }

  /**
   *
   * @param userId
   */
  public async getAllByUserId(userId) {
    return await this.notificationsRepository.find({
      where: {
        userId
      },
      select: ['id', 'title', 'message', 'read', 'createdAt'],
      order: {id: 'DESC'}
    })
  }

  /**
   *
   * @param userId
   */
  public async getUnreadCountByUserId(userId) {
    return await this.notificationsRepository.count({
      userId,
      read: false
    })
  }

  /**
   *
   * @param userId
   * @param id
   */
  public async markAsRead(userId, id) {
    const notification = await this.notificationsRepository.findOne({ id });
    if (notification) {
      if (notification.userId === userId) {
        if (!notification.read) {
          notification.read = true;
          return await this.notificationsRepository.save(notification);
        } else {
          throw new BadRequestException('Notification is already read.')
        }
      } else {
        throw new ForbiddenException('Notification does not belong to user.');
      }
    } else {
      throw new NotFoundException('Notification not found.');
    }
  }
}
