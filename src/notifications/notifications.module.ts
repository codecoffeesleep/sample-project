import { Module }                  from '@nestjs/common';
import { NotificationsController } from './notifications.controller';
import { NotificationsService }    from './notifications.service';
import { AuthModule }              from 'src/auth/auth.module';
import { NotificationsRepository } from './notifications.repository';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forFeature([NotificationsRepository]),
    AuthModule
  ],
  controllers: [NotificationsController],
  providers: [NotificationsService],
  exports: [NotificationsService]
})
export class NotificationsModule {}
