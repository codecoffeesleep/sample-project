import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne, JoinColumn,
}               from 'typeorm';
import { User } from '../users/user.entity';

@Entity({
  name: 'notifications',
})
export class Notification {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'user_id',
  })
  userId: number;

  @Column({
    name: 'title',
  })
  title: string;

  @Column({
    name: 'message',
  })
  message: string;

  @Column({
    name: 'status',
  })
  status: string;

  @Column({
    name: 'is_read',
  })
  read: boolean;

  @CreateDateColumn({
    name: 'created_at',
    nullable: true,
  })
  createdAt: Date;

  @UpdateDateColumn({
    name: 'updated_at',
    nullable: true,
  })
  updatedAt: Date;

  @ManyToOne(type => User, user => user.userNotifications)
  @JoinColumn({ name: 'user_id' })
  user: User;

}
