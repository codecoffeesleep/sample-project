import { IsString, Matches, IsOptional } from 'class-validator';

export default class CheckDateDTO {
  @IsString()
  @IsOptional()
  @Matches(/^\d{4}-([0-9]|0[1-9]|1[0-2])-([0-9]|0[1-9]|[12][0-9]|3[01])$/)
  start_date: string;

  @IsString()
  @IsOptional()
  @Matches(/^\d{4}-([0-9]|0[1-9]|1[0-2])-([0-9]|0[1-9]|[12][0-9]|3[01])$/)
  end_date: string;
}
