import { IsString } from 'class-validator';

export default class LogoDTO {
  @IsString()
  logo: string;

}
