import { EntityRepository, Repository } from 'typeorm';
import { Merchant }                     from './merchant.entity';

@EntityRepository(Merchant)
export class MerchantsRepository extends Repository<Merchant> {

}
