import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
}                         from 'typeorm';
import { MerchantToUser } from '../acl/entites/merchantToUser.entity';
import { Wallet }         from '../wallets/wallet.entity';
import { QR }             from '../qrs/qr.entity';

@Entity({
  name: 'merchants',
})
export class Merchant {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'name',
  })
  name: string;

  @Column({
    name: 'logo',
    nullable: true,
  })
  logo: string;

  @Column({
    name: 'address',
    nullable: true,
  })
  address: string;

  @Column({
    type: 'point',
    nullable: true,
  })
  location: any;

  @Column({
    name: 'phone_number',
    nullable: true,
  })
  phoneNumber: string;

  @Column({
    name: 'settlement_dates',
    nullable: true,
  })
  settlementDates: string;

  @CreateDateColumn({
    name: 'created_at'
  })
  createdAt: Date;

  @UpdateDateColumn({
    name: 'updated_at'
  })
  updatedAt: Date;

  @OneToMany(type => MerchantToUser, merchantToUserEntity => merchantToUserEntity.merchant)
  merchantToUsers: MerchantToUser[];

  @OneToMany(type => Wallet, wallet => wallet.merchant)
  wallets: Wallet[];

  @OneToMany(type => QR, qr => qr.merchant)
  qrs: QR[];
}
