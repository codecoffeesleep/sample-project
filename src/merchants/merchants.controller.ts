import {
  Body,
  Controller,
  Get,
  Param,
  Post, Query,
  Req,
  Res,
  SetMetadata,
  UseGuards,
  Patch, HttpStatus,
}                               from '@nestjs/common';
import { Merchant }             from './merchant.entity';
import { Request, Response }    from 'express';
import { AuthGuard }            from '@nestjs/passport';
import { AclService }           from '../acl/acl.service';
import MerchantRole             from '../acl/constants/merchant-roles.enum';
import { CheckPermissionGuard } from '../acl/guards/checkPermission.guard';
import { WalletsService }       from '../wallets/wallets.service';
import { MerchantsService }     from './merchants.service';
import CheckDateDTO             from './dto/checkDate.dto';
import LogoDTO                  from './dto/logo.dto';
import { ControllerResponse }   from '../common/response-decorator/responses.interface';

@Controller('merchants')
@UseGuards(AuthGuard())
export class MerchantsController {

  constructor(
    private readonly aclService: AclService,
    private readonly walletService: WalletsService,
    private readonly merchantService: MerchantsService,
  ) {
  }

  @Post()
  async createNewMerchant(@Body() body) {
    const { name } = body;

    const merchant = new Merchant();
    merchant.name = name;
    // await merchant.save();
    //
    // const user = await User.findOne({ id: ownerId });
    //
    // const merchantToUserEntity = new MerchantToUser();
    // merchantToUserEntity.merchant = merchant;
    // merchantToUserEntity.user = user;
    // merchantToUserEntity.role = role;
    // await merchantToUserEntity.save();
    //
    // return merchantToUserEntity;
  }

  /**
   *
   * @param request
   */
  @Get(':resource_id')
  @UseGuards(CheckPermissionGuard)
  @SetMetadata('acl', {
      resource: Merchant,
      roles: [
        MerchantRole.OWNER,
        MerchantRole.CASHIER,
      ],
    },
  )
  async merchantProfile(@Req() request: Request, @Param('resource_id') merchantId): Promise<ControllerResponse> {
    const merchant = await this.merchantService.getMerchantProfile(merchantId);
    return {
      data: merchant,
    };
  }

  /**
   *
   * @param request
   * @param response
   * @param merchantId
   */
  @Get('/:resource_id/wallets')
  @UseGuards(CheckPermissionGuard)
  @SetMetadata('acl', {
      resource: Merchant,
      roles: [
        MerchantRole.OWNER,
        MerchantRole.CASHIER,
      ],
    },
  )
  public async allMerchantWallets(@Req() request: Request, @Res() response: Response, @Param('resource_id') merchantId) {
    const wallets = await this.walletService.getMerchantWallets(merchantId);
    return {
      httpStatus: HttpStatus.OK,
      data: wallets,
      total: wallets.length,
    };
  }

  /**
   *
   * @param request
   * @param response
   * @param merchantId
   * @param id
   */
  @Get('/:resource_id/wallets/:id')
  @UseGuards(CheckPermissionGuard)
  @SetMetadata('acl', {
      resource: Merchant,
      roles: [
        MerchantRole.OWNER,
        MerchantRole.CASHIER,
      ],
    },
  )
  public async singleMerchantWallet(@Req() request: Request, @Res() response: Response, @Param('resource_id') merchantId, @Param('id') id) {
    const wallet = await this.walletService.getSingleMerchantWallet(merchantId, id);
    return {
      httpStatus: HttpStatus.OK,
      data: wallet,
    };
  }

  /**
   *
   * @param request
   * @param response
   * @param merchantId
   * @param id
   */
  @Get('/:resource_id/income/last-settlement')
  @UseGuards(CheckPermissionGuard)
  @SetMetadata('acl', {
      resource: Merchant,
      roles: [
        MerchantRole.OWNER,
        MerchantRole.CASHIER,
      ],
    },
  )
  public async getIncomeFromLastSettlement(@Req() request: Request, @Res() response: Response, @Param('resource_id') merchantId) {
    const totalIncome = await this.merchantService.incomeFromLastSettlement(merchantId);
    return {
      httpStatus: HttpStatus.OK,
      data: { totalIncome },
    };
  }

  /**
   *
   * @param request
   * @param response
   * @param merchantId
   * @param id
   */
  @Get('/:resource_id/income/today')
  @UseGuards(CheckPermissionGuard)
  @SetMetadata('acl', {
      resource: Merchant,
      roles: [
        MerchantRole.OWNER,
        MerchantRole.CASHIER,
      ],
    },
  )
  public async getTodayIncome(@Req() request: Request, @Res() response: Response, @Param('resource_id') merchantId) {
    const todayIncome = await this.merchantService.todayIncome(merchantId);
    return {
      httpStatus: HttpStatus.OK,
      data: { todayIncome: todayIncome.sum, total: todayIncome.total },
    };
  }

  /**
   *
   * @param request
   * @param response
   * @param merchantId
   * @param id
   */
  @Get('/:resource_id/transactions/today')
  @UseGuards(CheckPermissionGuard)
  @SetMetadata('acl', {
      resource: Merchant,
      roles: [
        MerchantRole.OWNER,
        MerchantRole.CASHIER,
      ],
    },
  )
  public async getTodayTransactions(@Req() request: Request, @Res() response: Response, @Param('resource_id') merchantId) {
    const transactions = await this.merchantService.todayTransactions(merchantId);
    return {
      httpStatus: HttpStatus.OK,
      data: transactions,
      total: transactions.length,
    };
  }

  /**
   *
   * @param request
   * @param response
   * @param merchantId
   * @param checkDateDTO
   */
  @Get('/:resource_id/transactions')
  @UseGuards(CheckPermissionGuard)
  @SetMetadata('acl', {
      resource: Merchant,
      roles: [
        MerchantRole.OWNER,
        MerchantRole.CASHIER,
      ],
    },
  )
  public async getTransactions(
    @Req() request: Request,
    @Res() response: Response,
    @Param('resource_id') merchantId,
    @Query() checkDateDTO: CheckDateDTO,
  ) {
    const { start_date: startDate, end_date: endDate } = checkDateDTO;
    const transactions = await this.merchantService.transactions(merchantId, { startDate, endDate });
    return {
      httpStatus: HttpStatus.OK,
      data: transactions,
      total: transactions.length,
    };
  }

  /**
   *
   * @param request
   * @param response
   * @param merchantId
   * @param transactionId
   */
  @Get('/:resource_id/transactions/:transaction_id')
  @UseGuards(CheckPermissionGuard)
  @SetMetadata('acl', {
      resource: Merchant,
      roles: [
        MerchantRole.OWNER,
        MerchantRole.CASHIER,
      ],
    },
  )
  public async getSingleTransaction(
    @Req() request: Request,
    @Res() response: Response,
    @Param('resource_id') merchantId,
    @Param('transaction_id') transactionId,
  ) {
    const transaction = await this.merchantService.singleTransaction(merchantId, transactionId);
    return {
      httpStatus: HttpStatus.OK,
      data: transaction,
    };
  }

  /**
   *
   * @param request
   * @param response
   * @param merchantId
   */
  @Get('/:resource_id/settlements')
  @UseGuards(CheckPermissionGuard)
  @SetMetadata('acl', {
      resource: Merchant,
      roles: [
        MerchantRole.OWNER,
        MerchantRole.CASHIER,
      ],
    },
  )
  public async getSettlements(
    @Req() request: Request,
    @Res() response: Response,
    @Param('resource_id') merchantId,
  ) {
    const settlements = await this.merchantService.settlements(merchantId);
    return {
      httpStatus: HttpStatus.OK,
      data: settlements,
      total: settlements.length,
    };
  }

  /**
   *
   * @param request
   * @param response
   * @param merchantId
   * @param settlementId
   */
  @Get('/:resource_id/settlements/:settlement_id')
  @UseGuards(CheckPermissionGuard)
  @SetMetadata('acl', {
      resource: Merchant,
      roles: [
        MerchantRole.OWNER,
        MerchantRole.CASHIER,
      ],
    },
  )
  public async getSettlement(
    @Req() request: Request,
    @Res() response: Response,
    @Param('resource_id') merchantId,
    @Param('settlement_id') settlementId,
  ) {
    const settlement = await this.merchantService.singleSettlements(merchantId, settlementId);
    return {
      httpStatus: HttpStatus.OK,
      data: settlement,
    };
  }

  /**
   *
   * @param request
   * @param response
   * @param logoDTO
   */
  @Patch('/:resource_id/logo')
  @UseGuards(CheckPermissionGuard)
  @SetMetadata('acl', {
      resource: Merchant,
      roles: [
        MerchantRole.OWNER,
        MerchantRole.CASHIER,
      ],
    },
  )
  public async updateLogo(
    @Req() request: Request,
    @Res() response: Response,
    @Param('resource_id') merchantId,
    @Body() logoDTO: LogoDTO,
  ) {
    const { logo } = logoDTO;
    await this.merchantService.updateLogo(merchantId, logo);
    return {
      httpStatus: HttpStatus.ACCEPTED,
    };
  }
}
