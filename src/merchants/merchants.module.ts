import { Module, forwardRef }  from '@nestjs/common';
import { MerchantsController } from './merchants.controller';
import { MerchantsService }    from './merchants.service';
import { AuthModule }          from '../auth/auth.module';
import { AclModule }           from '../acl/acl.module';
import { TypeOrmModule }       from '@nestjs/typeorm';
import { MerchantsRepository } from './merchants.repository';
import { WalletsModule }       from '../wallets/wallets.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([MerchantsRepository]),
    AuthModule,
    AclModule,
    forwardRef(() => WalletsModule)
  ],
  controllers: [MerchantsController],
  providers: [MerchantsService],
  exports: [MerchantsService],
})
export class MerchantsModule {
}
