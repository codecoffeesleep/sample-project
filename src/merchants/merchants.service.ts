import { Injectable, NotFoundException } from '@nestjs/common';
import { Merchant }                      from './merchant.entity';
import { InjectRepository }              from '@nestjs/typeorm';
import { MerchantsRepository }           from './merchants.repository';
import { WalletsService }                from '../wallets/wallets.service';

@Injectable()
export class MerchantsService {

  /**
   *
   * @param merchantsRepository
   */
  constructor(
    @InjectRepository(MerchantsRepository)
    private readonly merchantsRepository: MerchantsRepository,
    private readonly walletsService: WalletsService,
  ) {
  }

  /**
   *
   * @param merchantId
   */
  public async findById(merchantId: number): Promise<Merchant> {
    const merchant = await this.merchantsRepository.findOne({ id: merchantId }, {
      relations: [
        'merchantToUsers',
      ],
    });
    if (merchant) {
      return merchant;
    }
    throw new NotFoundException('Merchant not found');
  }

  /**
   *
   * @param merchantId
   */
  public async incomeFromLastSettlement(merchantId: number) {
    return await this.walletsService.merchantIncomeFromLastSettlement(merchantId);
  }

  /**
   *
   * @param merchantId
   */
  public async getMerchantProfile(merchantId: number) {
    const merchant = await this.merchantsRepository.findOne({ id: merchantId });
    if (merchant) {
      return merchant;
    } else {
      throw new NotFoundException('Merchant not found.');
    }
  }

  /**
   *
   * @param merchantId
   */
  public async todayIncome(merchantId: number) {
    return await this.walletsService.merchantTodayIncome(merchantId);
  }

  /**
   *
   * @param merchantId
   */
  public async todayTransactions(merchantId: number) {
    return await this.walletsService.merchantTodayTransactions(merchantId);
  }

  /**
   *
   * @param merchantId
   * @param options
   */
  public async transactions(merchantId: number, options: any) {
    return await this.walletsService.merchantTransactions(merchantId, options);
  }

  /**
   *
   * @param merchantId
   * @param transactionId
   */
  public async singleTransaction(merchantId: number, transactionId: number) {
    return await this.walletsService.getSingleMerchantTransaction(merchantId, transactionId);
  }

  /**
   *
   * @param merchantId
   */
  public async settlements(merchantId: number) {
    return await this.walletsService.merchantSettlements(merchantId);
  }

  /**
   *
   * @param merchantId
   * @param settlementId
   */
  public async singleSettlements(merchantId: number, settlementId: number) {
    return await this.walletsService.merchantSingleSettlement(merchantId, settlementId);
  }

  /**
   *
   * @param merchantId
   * @param newLogo
   */
  public async updateLogo(merchantId: number, newLogo: string) {
    const merchant = await this.merchantsRepository.findOne({ id: merchantId });
    if (merchant) {
      merchant.logo = newLogo;
      return await this.merchantsRepository.save(merchant);
    } else {
      throw new NotFoundException('Merchant not found.');
    }
  }
}
