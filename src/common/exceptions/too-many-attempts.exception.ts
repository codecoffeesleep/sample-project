import { HttpException, HttpStatus } from '@nestjs/common';

export class TooManyAttemptsException extends HttpException {
  constructor(msg: string = 'TOO MANY REQUESTS') {
    super(msg, HttpStatus.TOO_MANY_REQUESTS);
  }
}
