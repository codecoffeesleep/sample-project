import { Repository }      from 'typeorm/index';
import { classToPlain }    from 'class-transformer';
import { FindManyOptions } from 'typeorm/find-options/FindManyOptions';

export default class BaseRepository<T> extends Repository<T> {
  public excludeSensitiveFields(object: T): T {
    return classToPlain(object) as T;
  }

  public async saveAndExcludeSensitiveFields(object: T): Promise<T> {
    return this.excludeSensitiveFields(await this.save(object));
  }

  public async findAndCountAndExcludeSensitiveFields(options?: FindManyOptions<T>): Promise<[T[], number]> {
    let [results, total] = await this.findAndCount(options);
    results = results.map(result => {
      return this.excludeSensitiveFields(result);
    });
    return [results, total];
  }
}
