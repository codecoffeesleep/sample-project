import { IsNumber, IsObject, IsOptional, Max } from 'class-validator';
import { Exclude, Expose, Transform }          from 'class-transformer';

const defaultValueDecorator = (defaultValue: number) => {
  if (defaultValue < 1) {
    defaultValue = 1;
  }
  return Transform((target: any) => target || defaultValue);
};

@Exclude()
export default class PaginationDto {

  @Expose()
  @Transform(page => parseInt(page))
  @IsNumber()
  @IsOptional()
  @defaultValueDecorator(1)
  page: number;

  @Expose()
  @Transform(limit => parseInt(limit))
  @IsNumber()
  @Max(20)
  @IsOptional()
  @defaultValueDecorator(20)
  limit: number;

  @IsOptional()
  @IsObject()
  filter: object;

}
