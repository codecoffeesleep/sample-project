import { Injectable }         from '@nestjs/common';
import { InjectRepository }   from '@nestjs/typeorm';
import { ContactsRepository } from './contacts.repository';
import { Contact }            from './contact.entity';

@Injectable()
export class ContactsService {

  constructor(
    @InjectRepository(ContactsRepository)
    private readonly contactsRepository: ContactsRepository
  ){
  }

  public async createTicket(data: any) {
    const newTicket = new Contact();
    newTicket.message = data.message;
    newTicket.user = data.userId;
    newTicket.status = data.status;
    return await this.contactsRepository.save(newTicket);
  }

}
