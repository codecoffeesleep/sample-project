import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne, JoinColumn,
}               from 'typeorm';
import { User } from '../users/user.entity';

@Entity({
  name: 'tickets',
})
export class Contact {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'message',
  })
  message: string;

  @Column({
    name: 'status',
  })
  status: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @ManyToOne(type => User, user => user.contacts)
  @JoinColumn({ name: 'user_id' })
  user: User;
}
