enum contactStatuses {
  READ = 'READ',
  UNREAD = 'UNREAD'
}

export default contactStatuses;
