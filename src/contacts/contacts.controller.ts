import { Controller, HttpStatus, Post, Req, Res, UseGuards } from '@nestjs/common';
import { Request, Response }                           from 'express';
import { AuthGuard }                                   from '@nestjs/passport';
import contactStatuses                                 from './constants/contactStatuses';

@Controller('tickets')
@UseGuards(AuthGuard())
export class ContactsController {

  constructor() {
  }


  @Post()
  public async createTicket(@Req() request: Request, @Res() response: Response) {
    const body = request.body;
    const user = request['user'];
    body.userId = user.id;
    body.status = contactStatuses.UNREAD;
    return {
      httpStatus: HttpStatus.ACCEPTED
    };
  }

}
