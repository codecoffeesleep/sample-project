import { EntityRepository, Repository } from 'typeorm';
import { Contact }                         from './contact.entity';

@EntityRepository(Contact)
export class ContactsRepository extends Repository<Contact> {

}
