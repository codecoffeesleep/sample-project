import { Body, Controller, Delete, Get, Patch, Post, Req, UseGuards } from '@nestjs/common';
import { UsersService }                                               from './users.service';
import { AuthGuard }                                                  from '@nestjs/passport';
import { Request }                                                    from 'express';
import UpdateProfileDto                                               from './dto/updateProfile.dto';
import SetPasswordDto                                                 from './dto/setPassword.dto';
import { I18n, I18nContext }                                          from 'nestjs-i18n';
import { ControllerResponse }                                         from '../common/response-decorator/responses.interface';
import UpdatePasswordDto                                              from './dto/updatePassword.dto';
import DeletePasswordDto                                              from './dto/deletePassword.dto';
import CheckResetPasswordDto                                          from './dto/CheckResetPassword.dto';
import CheckPasswordDto                                               from './dto/checkPassword.dto';

@Controller('users')
@UseGuards(AuthGuard())
export class UsersController {

  /**
   *
   * @param userService
   */
  constructor(private readonly userService: UsersService) {
  }

  /**
   *
   * @param request
   */
  @Get('me')
  public async getProfile(@Req() request: Request) {
    const user = request['user'];
    const profile = await this.userService.getProfile(user.id);
    return {
      data: profile,
    };
  }

  /**
   *
   * @param updateProfileDto
   * @param request
   */
  @Patch('me')
  public async updateProfile(@Body() updateProfileDto: UpdateProfileDto, @Req() request: Request) {
    const user = request['user'];
    const updatedUser = await this.userService.updateProfile(user.id, updateProfileDto);
    return {
      data: updatedUser,
    };
  }

  /**
   * Set password for the first time
   *
   * @param i18n
   * @param setPasswordDto
   * @param request
   */
  @Post('me/password')
  public async setPassword(@I18n() i18n: I18nContext, @Body() setPasswordDto: SetPasswordDto, @Req() request: Request): Promise<ControllerResponse> {
    const user = request['user'];
    const updatedUser = await this.userService.setPassword(user.id, setPasswordDto);
    return {
      message: await i18n.translate('general.DEFAULT_UPDATED_SUCCESSFULLY', {
        args: {
          subject: 'Password',
        },
      }),
      data: updatedUser,
    };
  }

  /**
   * Change current password
   *
   * @param i18n
   * @param updatePasswordDto
   * @param request
   */
  @Patch('me/password')
  public async updatePassword(@I18n() i18n: I18nContext, @Body() updatePasswordDto: UpdatePasswordDto, @Req() request: Request): Promise<ControllerResponse> {
    const user = request['user'];
    const updatedUser = await this.userService.updatePassword(user.id, updatePasswordDto);
    return {
      message: await i18n.translate('general.DEFAULT_UPDATED_SUCCESSFULLY', {
        args: {
          subject: 'Password',
        },
      }),
      data: updatedUser,
    };
  }

  /**
   * Delete current password
   *
   * @param i18n
   * @param deletePasswordDto
   * @param request
   */
  @Delete('me/password')
  public async removePassword(@I18n() i18n: I18nContext, @Body() deletePasswordDto: DeletePasswordDto, @Req() request: Request): Promise<ControllerResponse> {
    const user = request['user'];
    const updatedUser = await this.userService.deletePassword(user.id, deletePasswordDto);
    return {
      message: await i18n.translate('general.DEFAULT_UPDATED_SUCCESSFULLY', {
        args: {
          subject: 'Password',
        },
      }),
      data: updatedUser,
    };
  }

  /**
   * Check current password
   *
   * @param i18n
   * @param checkPasswordDto
   * @param request
   */
  @Post('me/password/check')
  public async checkPassword(@I18n() i18n: I18nContext, @Body() checkPasswordDto: CheckPasswordDto, @Req() request: Request): Promise<ControllerResponse> {
    const user = request['user'];
    return {
      data: {
        'isCorrect': await this.userService.checkPassword(user.id, checkPasswordDto),
      },
    };
  }

  /**
   * Request to reset the password
   *
   * @param i18n
   * @param request
   */
  @Post('me/password/reset/send-code')
  public async sendResetPasswordCode(@I18n() i18n: I18nContext, @Req() request: Request): Promise<ControllerResponse> {
    const user = request['user'];
    const updatedUser = await this.userService.sendResetPasswordCode(user.id);
    return {
      message: await i18n.translate('general.DEFAULT_UPDATED_SUCCESSFULLY', {
        args: {
          subject: 'Password',
        },
      }),
      data: updatedUser,
    };
  }

  /**
   * Check the code of reset password
   *
   * @param i18n
   * @param checkResetPasswordDto
   * @param request
   */
  @Post('me/password/reset')
  public async checkResetPasswordCode(@I18n() i18n: I18nContext, @Body() checkResetPasswordDto: CheckResetPasswordDto, @Req() request: Request): Promise<ControllerResponse> {
    const user = request['user'];
    const updatedUser = await this.userService.checkResetPasswordCode(user.id, checkResetPasswordDto);
    return {
      message: await i18n.translate('general.DEFAULT_UPDATED_SUCCESSFULLY', {
        args: {
          subject: 'Password',
        },
      }),
      data: updatedUser,
    };
  }

}

