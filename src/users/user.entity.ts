import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
}                                             from 'typeorm';
import { Contact }                            from '../contacts/contact.entity';
import { Wallet }                             from '../wallets/wallet.entity';
import { Notification }                       from '../notifications/notification.entity';
import { MerchantToUser }                     from '../acl/entites/merchantToUser.entity';
import { CompanyToUser }                      from '../acl/entites/companyToUser.entity';
import { AfterLoad, AfterUpdate, BaseEntity } from 'typeorm/index';
import { Exclude }                            from 'class-transformer';

@Entity({
  name: 'users',
})
export class User {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'first_name',
    nullable: true,
  })
  firstName: string;

  @Column({
    name: 'last_name',
    nullable: true,
  })
  lastName: string;

  @Column({
    name: 'username',
    nullable: true,
  })
  username: string;

  @Column({
    name: 'password',
    nullable: true,
  })
  @Exclude()
  password: string;

  hasPassword: boolean;

  @Column({
    name: 'mobile_number',
  })
  mobileNumber: string;

  @Column({
    name: 'image',
    nullable: true,
  })
  image: string;

  @Column({
    name: 'refresh_token',
    nullable: true,
  })
  refreshToken: string;

  @CreateDateColumn({
    name: 'created_at',
    nullable: true,
  })
  createdAt: Date;

  @UpdateDateColumn({
    name: 'updated_at',
    nullable: true,
  })
  updatedAt: Date;

  // @ts-ignore
  @OneToMany(type => CompanyToUser, userCompany => userCompany.user)
  userCompanies: CompanyToUser[];

  // @ts-ignore
  @OneToMany(type => MerchantToUser, merchantToUser => merchantToUser.user)
  merchantToUsers: MerchantToUser[];

  // @ts-ignore
  @OneToMany(type => Contact, contact => contact.user)
  contacts: Contact[];

  // @ts-ignore
  @OneToMany(type => Notification, notification => notification.user)
  userNotifications: Notification[];

  @OneToMany(type => Wallet, wallet => wallet.user)
  wallets: Wallet[];

  @AfterLoad()
  public checkHasPasswordAfterLoad() {
    this.hasPassword = this.password !== null && this.password !== undefined && this.password.length > 0;
  }

  @AfterUpdate()
  public checkHasPasswordAfterUpdate() {
    this.checkHasPasswordAfterLoad();
  }

}
