import { forwardRef, Module }       from '@nestjs/common';
import { UsersController }          from './users.controller';
import { TypeOrmModule }            from '@nestjs/typeorm';
import { UsersRepository }          from './users.repository';
import { AuthModule }               from '../auth/auth.module';
import { UsersService }             from './users.service';
import UserNotificationRepository   from 'src/user-notification/userNotification.repository';
import ResetPasswordService         from './resetPassword.service';
import { SmsModule }                from '../sms/sms.module';
import { WalletsModule }            from '../wallets/wallets.module';
import { MerchantToUserRepository } from '../acl/merchantToUser.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([UsersRepository, UserNotificationRepository, MerchantToUserRepository]),
    forwardRef(() => AuthModule),
    forwardRef(() => WalletsModule),
    SmsModule,
  ],
  controllers: [UsersController],
  providers: [UsersService, ResetPasswordService],
  exports: [UsersService],
})
export class UsersModule {
}
