import { EntityRepository } from 'typeorm';
import { User }             from './user.entity';
import BaseRepository       from '../common/repositories/base.repository';

@EntityRepository(User)
export class UsersRepository extends BaseRepository<User> {

}
