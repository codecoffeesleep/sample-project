import { IsString, MinLength } from 'class-validator';

export default class DeletePasswordDto {
  @IsString()
  @MinLength(4)
  password: string;
}
