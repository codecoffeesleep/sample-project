import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export default class RegisterUserDto {
  @IsString()
  @IsNotEmpty()
  mobileNumber: string;

  @IsString()
  @IsOptional()
  firstName?: string;

  @IsString()
  @IsOptional()
  lastName?: string;

  @IsString()
  @IsOptional()
  image?: string;

}
