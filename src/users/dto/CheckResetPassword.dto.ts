import { IsNotEmpty, IsString, Matches, MinLength } from 'class-validator';

export default class CheckResetPasswordDto {
  @IsString()
  @IsNotEmpty()
  @Matches(/[0-9]{4}/)
  verificationCode: string;

  @IsString()
  @MinLength(4, {
    message: 'New password is too short',
  })
  newPassword: string;
}
