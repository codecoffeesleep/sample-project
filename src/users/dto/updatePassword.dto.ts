import { IsString, MinLength } from 'class-validator';

export default class UpdatePasswordDto {
  @IsString()
  @MinLength(4)
  currentPassword: string;

  @IsString()
  @MinLength(4, {
    message: 'New password is too short',
  })
  newPassword: string;
}
