import { IsString, MinLength } from 'class-validator';

export default class CheckPasswordDto {
  @IsString()
  @MinLength(4)
  password: string;
}
