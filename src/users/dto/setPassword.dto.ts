import { IsString, MinLength } from 'class-validator';

export default class SetPasswordDto {
  @IsString()
  @MinLength(4,{
    message: 'Password is too short'
  })
  password: string;
}
