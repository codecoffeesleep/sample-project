import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  NotFoundException, OnModuleInit,
  UnauthorizedException,
}                                   from '@nestjs/common';
import { InjectRepository }         from '@nestjs/typeorm';
import { User }                     from './user.entity';
import RegisterUserDto              from './dto/registerUser.dto';
import { UsersRepository }          from './users.repository';
import UserNotificationRepository
                                    from 'src/user-notification/userNotification.repository';
import UserNotificationStatuses
                                    from 'src/user-notification/constants/userNotificationStatuses.enum';
import { UserNotification }         from '../user-notification/userNotification.entity';
import UpdateProfileDto             from './dto/updateProfile.dto';
import SetPasswordDto               from './dto/setPassword.dto';
import UpdatePasswordDto            from './dto/updatePassword.dto';
import DeletePasswordDto            from './dto/deletePassword.dto';
import CheckResetPasswordDto
                                    from './dto/CheckResetPassword.dto';
import ResetPasswordService         from './resetPassword.service';
import SmsType                      from '../sms/constants/smsTypes.enum';
import { SmsService }               from '../sms/sms.service';
import CheckPasswordDto             from './dto/checkPassword.dto';
import { WalletsService }           from '../wallets/wallets.service';
import { ModuleRef }                from '@nestjs/core';
import { MerchantToUserRepository } from '../acl/merchantToUser.repository';

const bcrypt = require('bcrypt');

@Injectable()
export class UsersService implements OnModuleInit {

  private walletsService: WalletsService;

  constructor(
    @InjectRepository(UsersRepository)
    private readonly usersRepository: UsersRepository,
    @InjectRepository(MerchantToUserRepository)
    private readonly merchantToUserRepository: MerchantToUserRepository,
    private readonly userNotificationRepository: UserNotificationRepository,
    private readonly resetPasswordService: ResetPasswordService,
    private readonly smsService: SmsService,
    private readonly moduleRef: ModuleRef,
  ) {
  }

  /**
   * Load circular dependency modules
   *
   */
  onModuleInit() {
    this.walletsService = this.moduleRef.get(WalletsService, { strict: false });
  }

  /**
   *
   * @param id
   */
  public async findById(id: number): Promise<User> {
    const user = await this.usersRepository.findOneOrFail({ id });
    // Remove sensitive fields
    return this.usersRepository.excludeSensitiveFields(user);
  }

  /**
   *
   * @param mobileNumber
   * @param throwError
   */
  public async findByMobileNumber(mobileNumber: string, throwError: boolean = true): Promise<User> {
    const user = await this.usersRepository.findOneOrFail({
        where: { mobileNumber },
      },
    );

    if (user) {
      // Remove sensitive fields
      return this.usersRepository.excludeSensitiveFields(user);
    }
    if (!throwError) {
      return null;
    }

    throw new NotFoundException('User Not Found');
  }

  /**
   * Register a user and add a new wallet for him
   *
   * @param registerUserDto
   */
  public async registerNewUser(registerUserDto: RegisterUserDto): Promise<User> {
    const user = new User();
    Object.assign(user, registerUserDto);
    await this.usersRepository.saveAndExcludeSensitiveFields(user);

    // Add a new wallet to this user
    await this.walletsService.addPersonalWallet(user.id);

    // Remove sensitive fields
    return this.usersRepository.excludeSensitiveFields(user);
  }

  /**
   * Get current user's profile
   *
   * @param userId
   */
  public async getProfile(userId: number): Promise<User> {
    try {
      const user = await this.findById(userId);
      return this.usersRepository.excludeSensitiveFields(user);
    } catch (e) {
      throw new UnauthorizedException();
    }
  }

  /**
   * Update profile info
   *
   * @param id
   * @param updateProfileDto
   */
  public async updateProfile(id: number, updateProfileDto: UpdateProfileDto): Promise<User> {
    const user = await this.findById(id);

    // Extract normal attributes
    Object.assign(user, {
      firstName: updateProfileDto.firstName,
      lastName: updateProfileDto.lastName,
      image: updateProfileDto.image,
    });

    /**
     * Update username, we have 2 issues about username:
     * - username must be unique
     * - users can not change their usernames
     *
     */
    const username = updateProfileDto.username;
    if (username && !user.username) {
      const isAnyUserExistsWithThisUsername = await this.usersRepository.findOne({ username });
      if (!isAnyUserExistsWithThisUsername) {
        user.username = username;
      } else {
        throw new BadRequestException('Username is not valid, please change it');
      }
    }

    // Update playerId
    const playerId = updateProfileDto.playerId;
    if (playerId) {
      const userNotificationRecord = await this.userNotificationRepository.findOne({ playerId });
      if (!userNotificationRecord) {
        const userNotification = new UserNotification();
        userNotification.userId = id;
        userNotification.playerId = playerId;
        userNotification.status = UserNotificationStatuses.ACTIVE;
        await this.userNotificationRepository.save(userNotification);
      }
    }

    return await this.usersRepository.saveAndExcludeSensitiveFields(user);
  }

  /**
   * Set password, if user has not any active password.
   *
   * @param id
   * @param setPasswordDto
   */
  public async setPassword(id: number, setPasswordDto: SetPasswordDto): Promise<User> {
    const user = await this.findById(id);
    if (user.password) {
      throw new BadRequestException('You already have an active password');
    }

    // Hash the password
    user.password = await bcrypt.hash(setPasswordDto.password, 10);

    // Update the user object and return updated user
    return await this.usersRepository.saveAndExcludeSensitiveFields(user);
  }

  /**
   * Change the current password with receiving new password
   *
   * @param id
   * @param updatePasswordDto
   */
  public async updatePassword(id: number, updatePasswordDto: UpdatePasswordDto): Promise<User> {
    const user = await this.findById(id);

    if (!user.password) {
      throw new BadRequestException('You have not any active password');
    }

    // Check current password
    const isCurrentPasswordValid = await bcrypt.compare(updatePasswordDto.currentPassword, user.password);
    if (!isCurrentPasswordValid) {
      throw new BadRequestException('Current password is not valid');
    }

    // Check newPassword and currentPassword
    const isSame = await bcrypt.compare(updatePasswordDto.newPassword, user.password);
    if (isSame) {
      throw new BadRequestException('Current and new passwords are same! Please change the new password');
    }

    // Hash the new password
    user.password = await bcrypt.hash(updatePasswordDto.newPassword, 10);

    // Update the user object and return updated user
    return await this.usersRepository.saveAndExcludeSensitiveFields(user);
  }

  /**
   * Remove current password
   *
   * @param id
   * @param deletePasswordDto
   */
  public async deletePassword(id: number, deletePasswordDto: DeletePasswordDto): Promise<User> {
    const user = await this.findById(id);

    // Check current password
    const isCurrentPasswordValid = await bcrypt.compare(deletePasswordDto.password, user.password);
    if (!isCurrentPasswordValid) {
      throw new BadRequestException('Current password is not valid');
    }

    // Reset the password
    user.password = null;

    // Update the user object and return updated user
    return await this.usersRepository.saveAndExcludeSensitiveFields(user);
  }

  /**
   * Send a code regarding reset password
   *
   * @param id
   */
  public async sendResetPasswordCode(id: number): Promise<any> {
    const { mobileNumber, password } = await this.findById(id);

    if (!password) {
      throw new BadRequestException('You have not any active password');
    }

    const code = await this.resetPasswordService.storeNewAttempt(id);
    if (code) {

      // Send a sms
      const isSmsSent = await this.smsService.send(
        SmsType.RESET_PASSWORD,
        mobileNumber,
        `Security code for SigmaPay:\n${ code }`,
      );

      if (isSmsSent) {
        // Mask the user's mobileNumber
        const maskedMobileNumber = mobileNumber
          .replace(/\d(?=\d{4})/g, '*')
          .replace('+', '*');

        // Update the user object and return updated user
        return {
          mobileNumber: maskedMobileNumber,
        };
      }
    }

    throw new InternalServerErrorException('Something is wrong during reset the password');
  }

  /**
   * Check the code regarding reset password,
   * if the code is valid, change the password to the received password.
   *
   * @param id
   * @param checkResetPasswordDto
   */
  public async checkResetPasswordCode(id: number, checkResetPasswordDto: CheckResetPasswordDto): Promise<User> {
    const user = await this.findById(id);
    const { verificationCode, newPassword } = checkResetPasswordDto;

    const isCodeValid = await this.resetPasswordService.checkLastAttempt(id, verificationCode);

    if (isCodeValid) {
      await this.resetPasswordService.clearAttempt(id);
      user.password = await bcrypt.hash(newPassword, 10);
      return await this.usersRepository.saveAndExcludeSensitiveFields(user);
    }

    throw new BadRequestException('Code is not valid');
  }

  /**
   * Check current password
   *
   * @param id
   * @param checkPasswordDto
   */
  public async checkPassword(id: number, checkPasswordDto: CheckPasswordDto): Promise<boolean> {
    const user = await this.findById(id);

    if (user.hasPassword) {
      // Check current password
      return await bcrypt.compare(checkPasswordDto.password, user.password);
    }

    return false;
  }

  /**
   * Get related merchants
   *
   * @param id
   */
  public async getRelatedMerchants(id: number): Promise<any> {
    const merchant2Users = await this.merchantToUserRepository.getRelatedMerchants(id);
    return merchant2Users.map(m2u => {
      return m2u.merchant;
    });
  }
}
