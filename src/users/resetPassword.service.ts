import { Injectable }          from '@nestjs/common';
import * as bluebird           from 'bluebird';
import { ConfigService }       from '@nestjs/config';
import * as cryptoRandomString from 'crypto-random-string';

@Injectable()
export default class ResetPasswordService {

  private readonly redisClient: any;

  constructor() {
    const redis = require('redis');
    bluebird.promisifyAll(redis);

    const configService: ConfigService = new ConfigService();

    this.redisClient = redis.createClient({
      port: configService.get<number>('REDIS_PORT'),
      host: configService.get<string>('REDIS_HOST'),
      auth_pass: configService.get<string>('REDIS_PASSWORD'),
      no_ready_check: true,
    });
  }

  /**
   * Store new reset-password attempt
   *
   * @param userId
   */
  async storeNewAttempt(userId: number): Promise<string | boolean> {
    // Create a key, based on user's id
    const key = `reset_password_${ userId }`;

    // Generate new attempt code
    const code = this.generateNewAttempt();

    // Store in redis
    // This record will remove after 300 seconds
    return await this.redisClient.set(key, code, 'EX', 300) ? code : false;
  }

  /**
   * Check the received code is valid or not
   *
   * @param userId
   * @param code
   */
  public async checkLastAttempt(userId: number, code: string): Promise<boolean> {
    const key = `reset_password_${ userId }`;
    const lastAttempt = await this.redisClient.getAsync(key);
    return lastAttempt === code;
  }

  /**
   *
   */
  private generateNewAttempt(): string {
    return cryptoRandomString({
      length: 4,
      type: 'numeric',
    });
  }

  /**
   * Remove user's attempts
   *
   * @param userId
   */
  public async clearAttempt(userId: number): Promise<any> {
    const key = `reset_password_${ userId }`;
    return await this.redisClient.del(key);
  }
}
